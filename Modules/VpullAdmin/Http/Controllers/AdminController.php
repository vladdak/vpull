<?php

namespace Modules\VpullAdmin\Http\Controllers;

use App\Models\ContestReward;
use App\Models\Report;
use App\User;
use App\Models\Video;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;


class AdminController extends Controller
{
    use ValidatesRequests;

    protected $guard = 'admin';
    protected $redirectTo = '/vpulladmin/home';
    protected $redirectAfterLogout = '/vpulladmin/login';

    public function __construct(Video $video, User $user)
    {
        $this->video = $video;
        $this->user  = $user;

        $this->middleware('Modules\VpullAdmin\Http\Middleware\Authenticate:admin');
    }

    public function getIndexPage()
    {
        return view("vpulladmin::index");
    }

    public function getVideos()
    {
        $videos = Video::paginate(15);

        return view("vpulladmin::videos")->with(compact('videos'));
    }

    public function getVideo($uid)
    {
        $video = Video::where('uid', $uid)->withHidden()->first();

        return view("vpulladmin::video")->with(compact('video'));
    }

    public function postDeleteVideo($uid)
    {
        $video = Video::where('uid', $uid)->withHidden()->first();
        $video->delete();

        return redirect()->route('video.all');
    }

    public function getUsers()
    {
        $users = User::paginate(15);

        return view("vpulladmin::users")->with(compact('users'));
    }

    public function postDeleteUser($user_id)
    {
        $user = User::find($user_id);
        $user->delete();

        return redirect()->route('user.all');
    }

    public function getReports()
    {
        $reports = Report::orderBy('created_at')->paginate(15);

        return view('vpulladmin::reports')->with(compact('reports'));
    }

    public function postDeleteReport($report_id)
    {
        $report = Report::find($report_id);
        $report->delete();
        
        return redirect()->route('report.all');
    }

    public function getContestDetails()
    {
        $reward = ContestReward::first()->amount ?? 0;
        
        return view("vpulladmin::contest")->with(compact('reward'));
    }

    public function postSetContestReward(Request $request)
    {
        $this->validate($request, [
            'reward' => 'required|numeric'
        ]);

        ContestReward::create(['amount' => $request->get('reward')]);

        return redirect()->route('home');
    }
}
