<?php

namespace Modules\VpullAdmin\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class AdminAuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'admin';

    public function __construct()
    {
//        $this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
    }

    public function getLogin()
    {
        return view('vpulladmin::login');
    }
    
    public function postLogin(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        if (\Auth::guard('admin')->attempt(compact('username', 'password')))
        {
            return redirect()->route('home');
        }

        return redirect()->back()->withErrors(['auth' => 'Invalid user credentials']);
    }
    
    public function getLogout()
    {
        \Auth::guard('admin')->logout();

        return redirect()->route('home');
    }
    
}