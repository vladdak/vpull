<?php

Route::group(['middleware' => 'web', 
              'prefix'     => 'vpulladmin',
              'namespace'  => 'Modules\VpullAdmin\Http\Controllers'], function() 
{

    Route::get('login', [
        'as'   => 'login.show',
        'uses' => 'AdminAuthController@getLogin'
    ]);

    Route::post('login',  [
        'as'   => 'login.send',
        'uses' => 'AdminAuthController@postLogin'
    ]);

    Route::get('logout', [
        'as'   => 'logout',
        'uses' => 'AdminAuthController@getLogout'
    ]);

    Route::get('/', [
        'as'   => 'home',
        'uses' => 'AdminController@getContestDetails'
    ]);

    Route::group(['prefix' => 'contest'], function () {

        Route::get('/', [
            'as'   => 'contest.details',
            'uses' => 'AdminController@getContestDetails'
        ]);

        Route::post('set', [
            'as'   => 'contest.set',
            'uses' => 'AdminController@postSetContestReward'
        ]);
    });

    Route::group(['prefix' => 'report'], function () {

        Route::get('all', [
            'as'   => 'report.all',
            'uses' => 'AdminController@getReports'
        ]);

        Route::post('{report_id}/delete', [
            'as'   => 'report.delete',
            'uses' => 'AdminController@postDeleteReport'
        ]);
    });

    Route::group(['prefix' => 'user'], function () {

        Route::get('/', [
            'as'   => 'user.all',
            'uses' => 'AdminController@getUsers'
        ]);

        Route::post('{user_id}/delete', [
            'as'   => 'user.delete',
            'uses' => 'AdminController@postDeleteUser'
        ]);
    });

    Route::group(['prefix' => 'video'], function () {
       
        Route::get('/', [
            'as'   => 'video.all',
            'uses' => 'AdminController@getVideos'
        ]);
        
        Route::get('{video_uid}/show', [
            'as'   => 'video.show',
            'uses' => 'AdminController@getVideo'
        ]);

        Route::post('{video_uid}/delete', [
            'as'   => 'video.delete',
            'uses' => 'AdminController@postDeleteVideo'
        ]);
    });
        
});
