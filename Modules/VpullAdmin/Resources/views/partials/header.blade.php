<div class="nav-wrapper teal lighten-2">
    <a href="{{ route('home') }}" class="brand-logo">VPull Admin</a>

    <ul id="nav-mobile" class="right hide-on-med-and-down">

        @if (Auth::guard('admin')->check())

            <?php $prefix = 'vpulladmin' ?>

            <li class="{{ Request::is($prefix) ? 'active' : '' }}">
                <a href="{{ route('home')}}">Contest</a>
            </li>
            <li class="{{ Request::is($prefix.'/video') ? 'active' : '' }}">
                <a href="{{ route('video.all') }}">Videos</a>
            </li>
            <li class="{{ Request::is($prefix.'/user') ? 'active' : '' }}">
                <a href="{{ route('user.all') }}">Users</a>
            </li>
            <li class="{{ Request::is($prefix.'/report/all') ? 'active' : '' }}">
                <a href="{{ route('report.all') }}">Reports</a>
            </li>
            <li>
                <a href="{{ route('logout') }}"><i class="material-icons right">exit_to_app</i>Logout</a>
            </li>
        @else
            <li>
                <a href="{{ route('login.show') }}"><i class="material-icons right">exit_to_app</i>Login</a>
            </li>
        @endif
    </ul>
</div>
