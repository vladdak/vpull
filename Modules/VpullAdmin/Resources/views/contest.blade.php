@extends('vpulladmin::layouts.master')

@section('content')

    <div class="card-panel">

        <div class="card-content">
            <h3 class="teal-text">Contest amount: <span class="grey-text lighten-3">${{ $reward }}</span></h3>
        </div>

        <div class="card-action">
            <div class="row">
            <div class="col s12 m5">
            <form action="{{ route('contest.set') }}" method="post">
                {{ csrf_field() }}
                <span class="teal-text lighten-2" style="font-size: 18px">Set contest reward:</span>

                <div class="input-field inline">
                    <input id="reward" name="reward" type="text" class="validate">
                    <label for="reward" data-error="wrong" data-success="right">Reward</label>
                </div>

                <span class="inline">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Set
                        <i class="material-icons right">send</i>
                    </button>
                </span>
            </form>
            </div>
            </div>
        </div>
    </div>

@endsection