@extends('vpulladmin::layouts.master')

@section('content')

    <ul class="collection">
        @foreach ($users as $user)
            <li class="collection-item avatar">

                <img src="http://www.lovemarks.com/wp-content/uploads/profile-avatars/default-avatar-plaid-shirt-guy.png"
                     alt="" class="circle">

                <span class="title">{{ $user->fullname() }}</span>

                <p>
                    {{$user->email}}<br>
                    {{$user->phone}}
                </p>

                <a href="#remove-modal-{{$user->id}}" class="secondary-content">
                    <i class="material-icons">close</i>
                </a>

            </li>

            <!-- Remove Modal Structure -->
            <div id="remove-modal-{{$user->id}}" class="modal">
                <div class="modal-content">
                    <h4>Attention!</h4>
                    <p>Are you sure you want delete {{ $user->fullname() }}</p>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{ route('user.delete', $user->id ) }}">
                        {{ csrf_field() }}
                        <button type="submit" class=" modal-action modal-close waves-effect waves-green btn-flat">
                            Delete
                        </button>
                    </form>
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
            </div>

        @endforeach
    </ul>

    @include('vpulladmin::pagination.pagination-material', ['paginator' => $users])

@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        $('.modal').modal();
    });
</script>
@endsection