@if ($paginator && $paginator->lastPage() > 1)

    <ul class="pagination">

        {{-- Previous page Button --}}
        @if ($paginator->currentPage() == 1)
            <li class="disabled">
                <a href="javascript:void(0)">
                    <i class="material-icons">chevron_left</i>
                </a>
            </li>
        @else
            <li class="waves-effect">
                <a href="{{ $paginator->url(1) }}">
                    <i class="material-icons">chevron_left</i>
                </a>
            </li>
        @endif

        {{-- Content Navigation --}}
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? 'active teal lighten-2' : 'waves-effect' }}">
                <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor

        {{-- Next page Button--}}
        @if ($paginator->currentPage() == $paginator->lastPage())
            <li class="disabled">
                <a href="javascript:void(0)">
                    <i class="material-icons">chevron_right</i>
                </a>
            </li>
        @else
            <li class="waves-effect">
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}">
                    <i class="material-icons">chevron_right</i>
                </a>
            </li>
        @endif
    </ul>
@endif