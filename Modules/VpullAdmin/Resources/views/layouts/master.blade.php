<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>VpullAdmin</title>

        @include('vpulladmin::partials.stylesheets')
        @include('vpulladmin::partials.scripts')

    </head>

    <body>
        <nav>
            @include('vpulladmin::partials.header')
        </nav>

        <div class="container">
            @if (count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="row">
                    <div class="card-panel red lighten-2">
                        <span class="center-align white-text">{{ $error }}</span>
                    </div>
                </div>
            @endforeach
            @endif
        </div>

        <div class="login-wrapper">
            @yield('login')
        </div>

        <div class="container">
            @yield('content')
        </div>

        <footer>
            @include('vpulladmin::partials.footer')
        </footer>
    </body>
</html>
