@extends('vpulladmin::layouts.master')
@section('content')

    <div class="row">
        <div class="col m12">

            {{-- Show video player--}}
            @if($video->processed)
                <div class="card-panel center-align">
                    <video class="responsive-video" controls>
                        <source src="{{ $video->url() }}" type="video/mp4" width="853" height="480">
                    </video>
                </div>
            @else
                <div class="card red">
                    <div class="card-content">
                        <p class="white-text">Video is under processing</p>
                    </div>
                </div>
            @endif

        </div>
    </div>

    <div class="row">
        <div class="col m12">
        <div class="card">

            <div class="card-content">
                <div class="row" style="margin-bottom: 0">

                    {{-- Video details --}}
                    <div class="col m6">
                        <span class="card-title grey-text text-darken-4">
                            {{ $video->title }}
                        </span>

                        <p class="grey-text text-darken-1">Uploaded by <a href="#">{{ $video->user->fullname() }}</a></p>
                        <p class="grey-text text-darken-1">Created at:  {{ $video->created_at }}</p>
                    </div>

                    {{-- Delete Video Button--}}
                    <div class="col m2 right" style="margin-top: 25px">
                        <form method="post" action="{{ route('video.delete', $video->uid )}}">
                            {{ csrf_field() }}
                            <button class="btn waves-effect waves-light" type="submit">
                                Delete
                                <i class="material-icons right">close</i>
                            </button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        </div>

        {{-- Video description --}}
        <div class="col m12">
            <div class="card-panel">
                {{$video->description}}
            </div>
        </div>

    </div>
@endsection