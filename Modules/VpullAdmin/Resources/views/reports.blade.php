@extends('vpulladmin::layouts.master')

@section('content')
    <ul class="collection">
        @foreach ($reports as $report)
            <li class="collection-item avatar">

                <span class="title">{{ $report->report_type->type }}</span>

                <p> Posted by: {{ $report->user->fullname() }} </p>
                <p> Video:
                    <a href="{{ route('video.show', $report->video->uid) }}">{{ $report->video->title }}</a>
                </p>

                <a href="#remove-modal-{{$report->id}}" class="secondary-content">
                    <i class="material-icons">close</i>
                </a>

            </li>

            <!-- Remove Modal Structure -->
            <div id="remove-modal-{{$report->id}}" class="modal">
                <div class="modal-content">
                    <h4>Attention!</h4>
                    <p>Are you sure you want delete this report?</p>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{ route('report.delete', $report->id) }}">
                        {{ csrf_field() }}
                        <button type="submit" class=" modal-action modal-close waves-effect waves-green btn-flat">
                            Delete
                        </button>
                    </form>
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
            </div>

        @endforeach
    </ul>

    @include('vpulladmin::pagination.pagination-material', ['paginator' => $reports])
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
    </script>
@endsection