@extends('vpulladmin::layouts.master')

@section('content')

    @foreach ($videos->chunk(3) as $chunk)
        <div class="row">

            @foreach ($chunk as $video)

                <div class="col s12 m4">
                    <div class="card hoverable">

                        <div class="card-image waves-effect waves-block waves-light">
                            <a href="{{ route('video.show', $video->uid) }}">
                                <img src="{{ $video->preview() }}" class="img-responsive" style="max-height: 200px;">
                            </a>
                        </div>

                        <div class="card-content">
                            <span class="card-title grey-text text-darken-4">{{ $video->title }}</span>
                            <p><a class="activator" href="javascript:void(0)">Show more</a></p>
                        </div>

                        <div class="card-reveal">
                            <div class="card-title grey-text text-darken-4">
                                <i class="material-icons right">close</i>
                            </div>
                            <div class="align-left card-title grey-text text-darken-4">
                                {{ $video->title }}
                            </div>
                            <p>{{ $video->description }}</p>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    <div class="row">
        <div class="center-align">
            @include('vpulladmin::pagination.pagination-material', ['paginator' => $videos])
        </div>
    </div>

@endsection


