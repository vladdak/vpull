@extends('vpulladmin::layouts.master')

@section('login')
<div class="valign-wrapper">
<div class="row">
    <form method="post" action="{{ route('login.send') }}">

        {{ csrf_field() }}
        
        <div class="row">
            <div class="input-field col s12">
                <input id="username" name="username" type="text" class="validate">
                <label for="username">Username</label>
            </div>

            <div class="input-field col s12">
                <input id="password" name="password" type="password" class="validate">
                <label for="password">Password</label>
            </div>

            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">Authorize
                    <i class="material-icons right">account_box</i>
                </button>
            </div>
        </div>
    </form>
</div>
</div>
@endsection