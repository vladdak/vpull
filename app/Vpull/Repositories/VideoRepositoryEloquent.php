<?php

namespace App\Vpull\Repositories;

use App\User;
use App\Models\Video;
use App\Jobs\UploadVideoToS3;
use App\Vpull\Traits\StorageTrait;
use App\Models\Scopes\ActiveScope;
use App\Contracts\VideoRepositoryContract;

use App\Vpull\Providers\SortProvider;
use App\Vpull\Resources\ImageResource;
use App\Vpull\Resources\VideoResource;
use App\Http\Requests\VideoUploadRequest;

use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\LengthAwarePaginator;

class VideoRepositoryEloquent implements VideoRepositoryContract
{
    use DispatchesJobs, StorageTrait;

    /**
     * @var Video
     */
    public $model;

    /**
     * @var VideoResource
     */
    public $video;

    /**
     * @var ImageResource
     */
    public $image;

    /**
     * VideoRepository constructor.
     *
     * @param  Video          $model
     * @param  VideoResource  $video_resource
     * @param  ImageResource  $image_resource
     */
    public function __construct(Video $model, VideoResource $video_resource, ImageResource $image_resource)
    {
        $this->model = $model;
        $this->video = $video_resource;
        $this->image = $image_resource;
    }

    public function relations()
    {
        $query = $this->model->with('user', 'comments');

        return $query;
    }

    /**
     * Get video url by provided unique id.
     *
     * @param  string  $uid
     * @param  bool    $with_hidden
     * @return Video
     */
    public function find(string $uid, bool $with_hidden=False)
    {
        $query = $this->model->where('uid', $uid);
        if ($with_hidden) $query->withHidden();

        return $query->firstOrFail();
    }

    /**
     * Return paginated response for the given query.
     *
     * @param   Builder  $query
     * @param   int      $per_page
     * @param   array    $params
     * @return  LengthAwarePaginator
     */
    public function paginate($query, $per_page = 15, $params = null)
    {
        return $query->paginate($per_page)->appends($params);
    }

    /**
     * Get feed with available videos in given order.
     *
     * @param  SortProvider  $sort
     * @return Paginator
     */
    public function getVideoFeed(SortProvider $sort)
    {
        $ordered = $sort->provide();

        $feed = $this->relations()->ordered($ordered['sort'], $ordered['order']);
        $feed = $this->paginate($feed, $ordered['per_page'], $ordered);

        return $feed;
    }

    /**
     * Upload video to S3 storage.
     *
     * @param  VideoUploadRequest  $request
     * @return string              $uid
     */
    public function upload(VideoUploadRequest $request)
    {
        $storage_path = $request->user()->storage();

        $this->video->load($request->file('video'));
        $this->image->load($request->file('image'));

        $this->image->setStoragePath($storage_path);
        $this->video->setStoragePath($storage_path);
        $this->video->setPreview($this->image);

        $request->file('video')->move($this->video->local_path, $this->video->full_name);
        $request->file('image')->move($this->video->preview->local_path, $this->image->full_name);

        $this->store($this->video, $request->user());
        $this->dispatch(new UploadVideoToS3($this->video, $request->user()));
        
        return $this->video->uid;
    }

    /**
     * Store uploaded file properties in Video model.
     *
     * @param  VideoResource  $resource
     * @param  User           $user
     * @return Video          $video
     */
    public function store($resource, $user)
    {
        $video = $user->videos()->create([
            'uid'     => $resource->uid,
            'title'   => $resource->name,
            'path'    => $resource->filepath('s3'),
            'preview' => $resource->preview->filepath('s3')
        ]);

        return $video;
    }

    /**
     * Delete video file from cloud storage and database.
     *
     * @param  Video  $video
     * @return bool
     */
    public function delete($video)
    {
        $deleted = $this->diskCloud()->delete($video->path, $video->preview);

        if ($deleted) {
            $video->delete();
        }

        return $deleted;
    }

    /**
     * Mark video with given uid as processed.
     * Used when video has been uploaded
     * to Amazon S3 cloud service.
     *
     * @param  string $uid
     * @return void
     */
    public function setProcessed($uid)
    {
        $video = $this->find($uid, $with_hidden = true);
        $video->update(['processed' => 1]);
    }

    /**
     * Get video owner by given video unique id.
     *
     * @param  string  $uid
     * @return User
     */
    public function getVideoOwner($uid)
    {
        return $this->find($uid)->user;
    }

    /**
     * Get list of videos uploaded by provided user.
     *
     * @param  User          $user
     * @param  SortProvider  $sort
     * @param  bool          $with_hidden
     * @return Paginator
     */
    public function getUserVideos(User $user, SortProvider $sort, bool $with_hidden=True)
    {
        $user_id = $user->id;
        $ordered = $sort->provide();

        $query = $this->model->where('user_id', $user_id);
        $query = $query->ordered($ordered['sort'], $ordered['order']);

        if ($with_hidden) $query = $query->withHidden();

        $videos = $this->paginate($query, $ordered['per_page'], $ordered);

        return $videos;
    }

    /**
     * Get comments for the given video in specific order.
     *
     * @param  Video         $video
     * @param  SortProvider  $sort
     * @return Paginator
     */
    public function getComments(Video $video, SortProvider $sort)
    {
        $ordered = $sort->provide();

        $comments = $video->comments()->ordered($ordered['sort'], $ordered['order']);
        $comments = $this->paginate($comments, $ordered['per_page'], $ordered);
        
        return $comments;
    }
}