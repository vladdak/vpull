<?php namespace App\Vpull\Repositories;

use Hash;
use App\User;
use App\Contracts\UserRepository;
use App\Contracts\VideoRepositoryContract;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class UserRepositoryEloquent implements UserRepository
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var VideoRepositoryContract
     */
    protected $video_repo;
    
    /**
     * UserRepositoryEloquent constructor.
     * 
     * @param User $user
     * @param VideoRepositoryContract $video_repo
     */
    public function __construct(User $user, VideoRepositoryContract $video_repo)
    {
        $this->user = $user;
        $this->video_repo = $video_repo;
    }

    /**
     * Create new user instance.
     *
     * @param  array  $payload
     * @return Model
     */
    public function create($payload)
    {
        $payload['password'] = Hash::make($payload['password']);

        return $this->user->create($payload);
    }

    /**
     * Delete users account by provided id.
     * @param $id
     */
    public function delete($id)
    {
        $this->user->destroy($id);
    }

    /**
     * Find user by provided id.
     *
     * @param  integer  $id
     * @return Model
     */
    public function find($id)
    {
        return $this->user->findOrFail($id);
    }

    /**
     * Get user from provided attributes or create him.
     *
     * @param  array  $attributes
     * @return Model
     */
    public function firstOrCreate($attributes)
    {
        if (is_null($instance = $this->user->where($attributes)->first()))
        {
            return $this->user->create($attributes);
        }

        return $instance;
    }

    /**
     * Get user by email address.
     *
     * @param  array $attributes
     * @return Model
     */
    public function get($attributes)
    {
        return $this->user->where($attributes)->get();
    }

    /**
     * Get user by multiple fields.
     *
     * @param  $attribute
     * @param  $value
     * @return Collection|static[]
     */
    public function getWith($attribute, $value)
    {
        return $this->user->where($attribute, $value)->get();
    }

    /**
     * Get the list of videos for provided user.
     *
     * @param  integer    $user_id
     * @return Collection
     */
    public function getUserVideos($user_id)
    {
        $user = $this->find($user_id);

        return $user->videos()->get();
    }

    /**
     * Update user account details with provided payload data.
     *
     * @param  integer  $id
     * @param  array    $payload
     * @return boolean
     */
    public function update($id, $payload)
    {
        $user = $this->user->findOrFail($id);
        $user->update($payload);
    }
}