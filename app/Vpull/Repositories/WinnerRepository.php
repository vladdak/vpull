<?php

namespace App\Vpull\Repositories;

use Carbon\Carbon;
use App\Models\Winner;
use App\Models\ContestVideo;
use App\Vpull\Providers\SortProvider;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;


class WinnerRepository
{
    /**
     * @var Winner
     */
    protected $model;

    /**
     * @var ContestRepository
     */
    protected $contest;

    /**
     * ContestRepository constructor.
     *
     * @param Winner             $winner
     * @param ContestRepository  $contest
     */
    public function __construct(Winner $winner, ContestRepository $contest)
    {
        $this->model   = $winner;
        $this->contest = $contest;
    }

    /**
     * Get all records of Contest model.
     *
     * @return Builder
     */
    public function all(): Builder
    {
        return $this->model->all();
    }

    /**
     * Eager loading of all available relations for Winner model.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function loadRelations(Builder $query = null): Builder
    {
        if ($query) {
            return $query->with('videos','videos.votes', 'users');
        }

        return $this->model->with('videos','videos.votes', 'users');
    }

    /**
     * Get today contest winner.
     *
     * @return Winner
     */
    public function getTodayWinner()
    {
        $today  = Carbon::today()->toDateString();
        $winner = $this->model->where('created_at', '>=', $today)->first();

        return $winner;
    }

    /**
     * Get contest winners for the last week.
     *
     * @param  SortProvider $params
     * @return LengthAwarePaginator
     */
    public function getLatestWinners(SortProvider $params, $period)
    {
        $params = $params->provide();

        $sort  = $params['sort'];
        $order = $params['order'];

        $winners = $this->model->where('created_at', '>=', $period)->ordered($sort, $order);
        $winners = $this->paginate($winners, $params['per_page'], $params);

        return $winners;
    }
    
    /**
     * Get all contest winners
     *
     * @param  SortProvider  $params
     * @return LengthAwarePaginator
     */
    public function getAllWinners(SortProvider $params)
    {
        $params = $params->provide();

        $sort  = $params['sort'];
        $order = $params['order'];

        $winners = $this->model->ordered($sort, $order);
        $winners = $this->paginate($winners, $params['per_page'], $params);

        return $winners;
    }

    /**
     * Return paginated response for the given query.
     *
     * @param   Builder  $query
     * @param   int      $per_page
     * @param   array    $params
     * @return  LengthAwarePaginator
     */
    public function paginate($query, $per_page = 15, $params = null)
    {
        return $query->paginate($per_page)->appends($params);
    }

    /**
     * Pick the daily contest winner.
     */
    public function pickWinner()
    {
        $video = $this->findWinner();
        $this->makeWinner($video);

        $this->contest->collectStats();         // Save contest videos to stats table.
        $this->contest->refreshContest();       // Truncate contest table.
    }
    
    /**
     * Find the video with highest rating and posted the last.
     * 
     * @return ContestVideo
     */
    protected function findWinner()
    {
        $participants = $this->contest->loadRelations();
        
        $winner = $participants->orderBy('rating', 'desc')
                               ->orderBy('created_at', 'desc')->firstOrFail();
        
        return $winner;
    }

    /**
     * Create winner record.
     * @param ContestVideo $video
     */
    protected function makeWinner(ContestVideo $video)
    {
        $this->model->create([
            'uid'      => $video->uid,
            'user_id'  => $video->user_id,
            'video_id' => $video->video_id,
        ]);
    }

}