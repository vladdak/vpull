<?php

namespace App\Vpull\Repositories;

use App\Contracts\VideoRepositoryContract;
use App\Models\Vote;
use App\Models\ContestVideo;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class VoteRepository
{
    /**
     * @var Vote
     */
    protected $model;

    /**
     * @var VideoRepositoryContract
     */
    protected $repository;

    /**
     * VoteRepository constructor.
     * 
     * @param VideoRepositoryContract $repository
     * @param Vote $model
     */
    public function __construct(VideoRepositoryContract $repository, Vote $model)
    {
        $this->repository = $repository;
        $this->model = $model;
    }

    /**
     * Get votes for the given video.
     *
     * @param  ContestVideo $video
     * @return array
     */
    public function getVotes(ContestVideo $video)
    {
        $votes['up']   = $video->upVotes();
        $votes['down'] = $video->downVotes();

        return $votes;
    }

    /**
     * Vote for the given video and increase video rating.
     *
     * @param ContestVideo $video
     * @param int          $user_id
     */
    public function voteUp(ContestVideo $video, int $user_id)
    {
        $vote = $video->votedBy($user_id);

        if ($vote == 'up') {
            throw new ConflictHttpException("You have already voted for this video");
        }

        if ($vote && $vote == 'down') {
            $this->unvote($video, $user_id);
        }

        $video->voteUp($user_id);
    }

    /**
     * Vote against the given video and decrease video rating.
     *
     * @param ContestVideo  $video
     * @param int           $user_id
     */
    public function voteDown(ContestVideo $video, int $user_id)
    {
        $vote = $video->votedBy($user_id);

        if ($vote == 'down') {
            throw new ConflictHttpException("You have already voted against this video");
        }

        if ($vote && $vote == 'up') {
            $this->unvote($video, $user_id);
        }

        $video->voteDown($user_id);
    }

    /**
     * Check if user already voted for the current video
     * and remove his vote if he did.
     *
     * @param  ContestVideo $video
     * @param  int          $user_id
     * @return bool
     */
    public function unvote(ContestVideo $video, int $user_id)
    {
        $has_voted = (bool) $video->votedBy($user_id);

        if ($has_voted) {
            $video->voteRemove($user_id);
        }

        return $has_voted;
    }
}