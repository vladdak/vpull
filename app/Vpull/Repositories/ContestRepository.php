<?php

namespace App\Vpull\Repositories;

use Carbon\Carbon;

use App\Models\Video;
use App\Models\Statistics;
use App\Models\ContestVideo;
use App\Vpull\Providers\SortProvider;
use App\Jobs\Video\PublishVideoOnContest;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ContestRepository
{
    /**
     * @var ContestVideo
     */
    protected $model;

    /**
     * ContestRepository constructor.
     *
     * @param ContestVideo $video
     */
    public function __construct(ContestVideo $video)
    {
        $this->model = $video;
    }

    /**
     * Get all records of Contest model.
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function feed(SortProvider $sort)
    {
        $order = $sort->provide();

        $feed = $this->loadRelations()->ordered($order['sort'], $order['order']);
        $feed = $this->paginate($feed, $order['per_page'], $order);

        return $feed;
    }

    /**
     * Eager loading of all available relations for ContestVideo model.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function loadRelations(Builder $query = null): Builder
    {
        if ($query) {
            return $query->with('video', 'votes', 'user');
        }

        return $this->model->with('video', 'votes', 'user');
    }

    /**
     * Publish video on daily voting.
     *
     * @param  Video  $video
     * @param  string $start_at
     *
     * @throws AccessDeniedHttpException
     * @throws ConflictHttpException
     *
     * @return Model
     */
    public function startContest(Video $video, string $start_at = "")
    {
        $winner = \App::make(WinnerRepository::class);

        if ($winner->wonThreeTimes($video->user_id)) {
            throw new AccessDeniedHttpException("You are unable to publish video on the contest until tomorrow.");
        }

        if ($video->onContest()) {
            throw new ConflictHttpException("Video is already on contest");
        }
        
        $job = (new PublishVideoOnContest($video));

        $start_at = new \DateTime($start_at);

        if ($start_at < new \DateTime('now')) {
            throw new UnprocessableEntityHttpException("Invalid date provided.");
        }

        if ($start_at) $job = $job->delay($start_at);

        $this->dispatch($job);
    }

    /**
     * Take out video from daily voting.
     *
     * @param  Video  $video
     * @return mixed
     */
    public function stopContest(Video $video)
    {
        $daily = $video->contest_video()->firstOrFail();

        return $daily->delete();
    }

    /**
     * Return paginated response for the given query.
     *
     * @param   Builder  $query
     * @param   int      $per_page
     * @param   array    $params
     * @return  LengthAwarePaginator
     */
    public function paginate($query, $per_page = 15, $params = null)
    {
        return $query->paginate($per_page)->appends($params);
    }

    /**
     * Collect all contest statistic in contest_stats table.
     */
    public function collectStats()
    {
        $participants   = $this->loadRelations();
        $yesterday_date = Carbon::yesterday()->toDateString();

        foreach ($participants as $video)
        {
            Statistics::create([

                'uid'        => $video->uid,
                'user_id'    => $video->user->id,
                'video_id'   => $video->video->id,
                'rating'     => $video->rating,
                'up_votes'   => $video->upVotes(),
                'down_votes' => $video->downVotes(),
                'created_at' => $yesterday_date,

            ]);
        }
    }

    /**
     * Truncate daily video contest table.
     * Used when after contest finish.
     */
    public function refreshContest()
    {
        \DB::table('contest_videos')->delete();
    }
}