<?php

namespace App\Vpull\Resources;

class VideoResource extends AbstractResource
{
    protected $uid;
    protected $preview;

    public function load($resource)
    {
        parent::load($resource);

        $this->uid = uniqid(true);
        $this->fullname = $this->uid . $this->fullname;

        $this->configCloudPath();
        $this->configLocalPath();
    }

    /**
     * Configure cloud storage directory location where file will be stored
     * for the current resource class.
     *
     */
    public function configCloudPath()
    {
        $this->cloud_path = $this->parent_path . '/video';
    }

    /**
     * Local storage directory location where video file will be stored until be
     * uploaded to S3 for the current resource class.
     *
     */
    public function configLocalPath()
    {
        $this->local_path = storage_path('app/temp/video/' . $this->name);
    }

    /**
     * Get video preview image.
     *
     * @return ImageResource
     * @throws \ErrorException
     */
    public function getPreview()
    {
        if ($this->preview !== null) {
            return $this->preview;
        }

        else {
            throw new \ErrorException("You must call setPreview() method first!");
        }
    }

    /**
     * Get video file unique id.
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set preview for the video resource.
     *
     * @param ImageResource $preview
     */
    public function setPreview(ImageResource $preview)
    {
        $this->preview = $preview;

        $this->preview->local_path = $this->local_path;
        $this->preview->cloud_path = $this->cloud_path;
        $this->preview->fullname   = $this->uid . $this->preview->fullname;
    }

    /**
     * Overriding setStoragePath() method to configure correct resource paths. 
     * 
     * @param string  $storage_path
     */
    public function setStoragePath($storage_path)
    {
        parent::setStoragePath($storage_path);

        $this->configCloudPath();
    }
}