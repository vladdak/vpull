<?php

namespace App\Vpull\Resources;

use App\Vpull\Traits\FileTrait;

abstract class AbstractResource implements ResourceView
{
    use FileTrait;

    protected $name;

    protected $cloud_path;

    protected $local_path;

    protected $parent_path;

    protected $fullname;

    protected $extension;
    

    /**
     * Magic getter for resource object.
     * Used to call methods as they were attributes.
     *
     * @param  string  $attr
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function __get($attr)
    {
        $method = 'get' . camel_case($attr);

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        else {
            throw new \InvalidArgumentException(sprintf("Unknown argument '%s'", $attr));
        }
    }

    /**
     * Magic setter for resource object.
     * Used to call methods as they were attributes.
     *
     * @param  string  $attr
     * @param  string  $param
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function __set($attr, $param)
    {
        $method = 'set' . camel_case($attr);

        if (method_exists($this, $method)) {
            return $this->$method($param);
        }

        else {
            throw new \InvalidArgumentException(sprintf("Unknown argument '%s'", $attr));
        }
    }

    public function configStoragePath()
    {
        $this->parent_path = 'uploaded';
        $this->cloud_path  = $this->parent_path . '/public';
        $this->local_path  = storage_path('app/uploaded/');
    }

    /**
     * Get path to video file that depends on storage driver.
     *
     * @param  string  $driver
     * @throws \InvalidArgumentException
     * @return string
     */
    public function filepath($driver='local')
    {
        if ($driver == 's3') {
            $path = $this->getCloudPath() . '/' . $this->getFullname();
        }

        elseif ($driver == 'local') {
            $path = $this->getLocalPath() . '/' . $this->getFullname();
        }

        else {
            throw new \InvalidArgumentException(sprintf("Unknown argument '%s'", $driver));
        }

        return $path;
    }

    /**
     * Cloud storage directory location where file will be stored.
     *
     * @return string
     */
    public function getCloudPath()
    {
        return $this->cloud_path;
    }

    /**
     * Get uploaded file extension.
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Get file name with extension
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Local storage directory location where video file will be stored until be
     * uploaded to S3.
     *
     * @return string
     */
    public function getLocalPath()
    {
        return $this->local_path;
    }

    /**
     * Get uploaded file name.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getParentPath()
    {
        return $this->parent_path;
    }

    /**
     * Set file resource.
     * @param $resource
     */
    public function load($resource)
    {
        $this->name = $this->getFilename($resource);
        $this->fullname = $this->getFileFullname($resource);
        $this->extension = $this->getFileExtension($resource);

        $this->configStoragePath();
    }


    /**
     * Change file full name.
     *
     * @return string
     */
    public function setFullname($name)
    {
        $this->fullname = $name;
    }
    
    /**
     * Cloud storage directory location where file will be stored.
     *
     * @return string
     */
    public function setCloudPath($cloud_path)
    {
       $this->cloud_path = $cloud_path;
    }

    /**
     * Local storage directory location where video file will be stored until be
     * uploaded to S3.
     *
     * @return string
     */
    public function setLocalPath($local_path)
    {
        $this->local_path = $local_path;
    }

    /**
     * Set parent path for all uploaded files.
     *
     * @param $storage_path
     */
    public function setStoragePath($storage_path)
    {
        $this->parent_path = $storage_path;
    }
}