<?php

namespace App\Vpull\Resources;

interface ResourceView
{
    public function load($resource);
}