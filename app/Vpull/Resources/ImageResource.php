<?php

namespace App\Vpull\Resources;

class ImageResource extends AbstractResource
{
    public function load($resource)
    {
        parent::load($resource);

        $this->configCloudPath();
        $this->configLocalPath();
    }

    /**
     * Configure cloud storage directory location where file will be stored
     * for the current resource class.
     *
     */
    public function configCloudPath()
    {
        $this->cloud_path = $this->parent_path . '/images';
    }

    /**
     * Local storage directory location where video file will be stored until be
     * uploaded to S3 for the current resource class.
     *
     */
    public function configLocalPath()
    {
        $this->local_path = storage_path('app/temp/images');
    }

    /**
     * Overriding setStoragePath() method to configure correct resource paths.
     *
     * @param string  $storage_path
     */
    public function setStoragePath($storage_path)
    {
        parent::setStoragePath($storage_path);

        $this->configCloudPath();
    }
}