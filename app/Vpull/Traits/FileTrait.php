<?php

namespace App\Vpull\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;

trait FileTrait
{

    /**
     * Get extension of uploaded file.
     *
     * @param  UploadedFile  $file
     * @return string
     */
    public function getFileExtension($file)
    {
        return $file->getClientOriginalExtension();
    }

    /**
     * Get file original name.
     *
     * @param  UploadedFile  $file
     * @return string
     */
    public function getFilename($file)
    {
        return pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
    }

    /**
     * Get file name with extension
     *
     * @param  UploadedFile  $file
     * @return string
     */
    public function getFileFullname($file)
    {
        return $file->getClientOriginalName();
    }

}