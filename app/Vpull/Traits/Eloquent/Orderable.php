<?php

namespace App\Vpull\Traits\Eloquent;

use App\Vpull\Providers\SortProvider;
use Illuminate\Database\Eloquent\Builder;

trait Orderable
{
    public function scopeOrdered(Builder $query, $sort, $order): Builder
    {
        $sort  = $this->checkSort($sort);    // Check if supported orderable field has been passed
        $order = $this->checkOrder($order);  // Check if supported order type has been passed

        return  $query->orderBy($sort, $order);
    }

    /*
    |--------------------------------------------------------------------------
    | Helper methods used for validating order request
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Validate passed order parameter.
     *
     * @param  string  $order
     * @return string
     * @throws \InvalidArgumentException
     */
    public function checkOrder(string $order)
    {
        if (!in_array($order, ['asc', 'desc']))
        {
            throw new \InvalidArgumentException("Order must be equal to 'asc' or 'desc' !");
        }

        return $order;
    }

    /**
     * Get field that can be used for ordering.
     *
     * @param  string  $sort
     * @return string
     * @throws \InvalidArgumentException
     */
    public function checkSort($sort)
    {
        if (!property_exists($this, 'sort')) {
            throw new \InvalidArgumentException("Current model doesn't support ordering");
        }

        if (array_key_exists($sort, $this->sort)) {
            return $this->sort[$sort];
        }

        else {
            throw new \InvalidArgumentException("Ordering by {$sort} field is not supported!");
        }
    }
}