<?php

namespace App\Vpull\Traits\Eloquent;

use Carbon\Carbon;

trait Datetime
{
    public function toCarbon($time, $format='Y-m-d H:i:s'): Carbon
    {
        return Carbon::createFromFormat($format, $time);
    }
    
    public function timezone(Carbon $date): string
    {
        return $date->getTimezone();
    }

    public function toDate(Carbon $date): string
    {
        return $date->toDateString();
    }

    public function toDatetime(Carbon $date): string
    {
        return $date->toDateTimeString();
    }

    public function toDaytime(Carbon $date): string
    {
        return $date->toDayDateTimeString();
    }

    public function toFriendly(Carbon $date): string
    {
        return $date->diffForHumans();
    }

    public function toTime(Carbon $date): string
    {
        return $date->toTimeString();
    }

    public function toTimestamp(Carbon $date): string
    {
        return $date->getTimestamp();
    }
}