<?php

namespace app\Vpull\Traits\Eloquent;


trait VideoRelations
{
    public function scopeWithComments($query)
    {
        return $query->with('comment');
    }

    public function scopeWithUsers($query)
    {
        return $query->with('user');
    }

    public function scopeWithVotes($query)
    {
        return $query->with('vote');
    }
}