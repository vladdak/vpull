<?php

namespace App\Vpull\Traits\Eloquent\User;

use App\Models\Winner;

trait WinnerRelation
{

    /**
     * Get wins for the current user.
     */
    public function winner()
    {
        return $this->hasMany(Winner::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Model action and state methods
    |--------------------------------------------------------------------------
    | There are described model actions or
    | behaviors that are not presented in Eloquent Model.
    |
    */

    /**
     * Check if the current user won 3 times in a row.
     * @return  bool
     */
    public function wonThreeTimes(): bool
    {
        $latest = $this->winner()->orderBy('created_at', 'desc')->take(3)->get();
        $count  = $latest->where('user_id', $this->id)->count();

        if ($count == 3) return true;

        return false;
    }
}