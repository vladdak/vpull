<?php

namespace App\Vpull\Traits\Eloquent;

use App\Models\Vote;

trait VideoVotes
{

    /**
     * Check if the video has been published on daily contest.
     *
     * @return bool
     */
    public function onContest()
    {
        if ($this->contest_video()->first()) {
            return true;
        }

        return false;
    }

    /**
     * Get up votes for the voteable instance.
     *
     * @return integer
     */
    public function upVotes()
    {
        return $this->votes()->where('vote', true)->count();
    }

    /**
     * Get down votes for the voteable instance.
     *
     * @return integer
     */
    public function downVotes()
    {
        return $this->votes()->where('vote', false)->count();
    }

    /**
     * Check if instance has been voted by given user.
     * 
     * Returns vote weight value if video has been voted by user.
     * If videos has not been voted by user 'false' will be returned.
     * If $weight param has been passed boolean value will be returned.
     *
     * @param  integer  $user_id
     * @param  string   $weight
     * @return bool | string 
     */
    public function votedBy($user_id, $weight=null)
    {
        $vote = $this->votes()->where('user_id', $user_id)->get();
        
        // Return 'false' if user never voted for the video.
        if ($vote->isEmpty()) {
            return false;
        }

        // Return 'true' if user up voted the video.
        if ($weight == 'up') {
            return $vote[0]->vote == true;
        }

        // Return false if user down voted the video.
        elseif ($weight == 'down') {
            return $vote[0]->vote == false;
        }

        // Otherwise return vote value.
        return Vote::$vote_weights[(string) $vote[0]->vote];
    }
}