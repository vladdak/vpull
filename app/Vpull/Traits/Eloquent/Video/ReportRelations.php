<?php

namespace App\Vpull\Traits\Eloquent\Video;

use App\Models\Report;

trait ReportRelations
{
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}