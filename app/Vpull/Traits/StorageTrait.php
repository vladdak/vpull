<?php

namespace App\Vpull\Traits;

use Storage;
use \Illuminate\Filesystem\FilesystemAdapter;

trait StorageTrait
{
    public function diskCloud(): FilesystemAdapter
    {
        return Storage::disk('s3-video');
    }

    public function diskLocal(): FilesystemAdapter
    {
        return Storage::disk('local');
    }

    public function diskPublic(): FilesystemAdapter
    {
        return Storage::disk('public');
    }

    public function diskTemp(): FilesystemAdapter
    {
        return Storage::disk('temp');
    }
}