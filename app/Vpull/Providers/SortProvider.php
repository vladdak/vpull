<?php

namespace App\Vpull\Providers;

use App\Http\Requests\OrderableRequest;

class SortProvider implements ProviderInterface
{
    protected $sort;

    protected $order;

    protected $per_page;

    public function __construct(OrderableRequest $request, $default_sort='date', $default_order='desc')
    {
        $this->sort     = $request->sort     ?? $default_sort;
        $this->order    = $request->order    ?? $default_order;
        $this->per_page = $request->per_page ?? 15;
    }

    public function provide()
    {
        return [
            'sort'     => $this->sort,
            'order'    => $this->order,
            'per_page' => $this->per_page
        ];
    }
}