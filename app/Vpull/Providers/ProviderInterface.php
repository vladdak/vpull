<?php

namespace App\Vpull\Providers;


interface ProviderInterface
{
    public function provide();
}