<?php

namespace App\Vpull\Transformers;

use App\Models\Winner;
use League\Fractal\TransformerAbstract;

class WinnerTransformer extends TransformerAbstract
{
    public function transform(Winner $winner)
    {
        return [
            'id' => $winner->uid,
            'feedback' => $winner->feedback,

            'url'         => $winner->video->url(),
            'title'       => $winner->video->title,
            'preview'     => $winner->video->preview(),
            'description' => $winner->video->description,

            'user_id'  => $winner->user_id,
            'username' => $winner->video->username(),

            'won_at' => $winner->toDate($winner->created_at),
            'posted_at_diff' => $winner->toFriendly($winner->created_at),
        ];
    }
}