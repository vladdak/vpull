<?php

namespace App\Vpull\Transformers;

use App\Models\ContestVideo;

use League\Fractal\TransformerAbstract;

class ContestVideoTransformer extends TransformerAbstract
{
    public function transform(ContestVideo $contest)
    {
        return [
            'id'      => $contest->uid,
            'url'     => $contest->video->url(),
            'title'   => $contest->video->title,
            'preview' => $contest->video->preview(),
            'description' => $contest->video->description,
            'rating'      => $contest->rating,
            'up_votes'    => $contest->upVotes(),
            'down_votes'  => $contest->downVotes(),
            'user_id'     => $contest->video->user_id,
            'username'    => $contest->video->username(),
            'user_photo'  => $contest->video->user->avatar(),
            'posted_at'      => $contest->toDatetime($contest->created_at),
            'posted_at_diff' => $contest->toFriendly($contest->created_at),
        ];
    }
}