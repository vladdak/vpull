<?php

namespace App\Vpull\Transformers;


use App\Models\Statistics;
use League\Fractal\TransformerAbstract;

class StatisticsTransformer extends TransformerAbstract
{
    public function transform(Statistics $stats)
    {
        return [
            'id' => $stats->uid,

            'url'         => $stats->video->url(),
            'title'       => $stats->video->title,
            'preview'     => $stats->video->preview(),
            'description' => $stats->video->description,

            'rating'     => $stats->rating,
            'up_votes'   => $stats->up_votes,
            'down_votes' => $stats->down_votes,

            'user_id'  => $stats->user_id,
            'username' => $stats->video->username(),

            'competed_at'    => $stats->toDate($stats->created_at),
            'posted_at_diff' => $stats->toFriendly($stats->created_at),
        ];
    }
}