<?php

namespace App\Vpull\Transformers;

use App\User;
use App\Models\Video;
use League\Fractal\TransformerAbstract;

class VideoTransformer extends TransformerAbstract
{
    public function transform(Video $video)
    {
        return [
            'id'             => $video->uid,
            'url'            => $video->url(),
            'title'          => $video->title,
            'preview'        => $video->preview(),
            'description'    => $video->description,
            'processed'      => (bool) $video->processed,
            'on_contest'     => $video->onContest(),
            'user_id'        => $video->user_id,
            'username'       => $video->username(),
            'posted_at'      => $video->toDatetime($video->created_at),
            'posted_at_diff' => $video->toFriendly($video->created_at),
        ];
    }
}