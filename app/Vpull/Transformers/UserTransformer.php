<?php

namespace App\Vpull\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'        => $user->id,
            'firstname' => $user->firstname,
            'lastname'  => $user->lastname,
            'photo'     => $user->avatar(),
            'email'     => $user->email,
            'phone'     => $user->phone,
        ];
    }
}