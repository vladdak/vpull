<?php

namespace App\Vpull\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentsTransformer extends TransformerAbstract
{
    public function transform(Comment $comment)
    {
        return [
            'id'   => $comment->uid,
            'body' => $comment->body,
            'user_id'   => (int) $comment->user_id,
            'username'  => $comment->commentator(),
            'posted_at' => $comment->toDatetime($comment->created_at),
            'posted_at_diff' => $comment->toFriendly($comment->created_at),
        ];
    }
}