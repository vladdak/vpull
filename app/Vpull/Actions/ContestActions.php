<?php

namespace App\Vpull\Actions;

use App\Models\Video;
use App\Contracts\VideoRepositoryContract;
use App\Vpull\Tasks\Contest\PublishVideoOnContestTask;

class ContestActions
{
    /**
     * ContestActions constructor.
     * 
     * @param PublishVideoOnContestTask $publishVideoOnContestTask
     * @param VideoRepositoryContract   $videoRepository
     */
    public function __construct(PublishVideoOnContestTask $publishVideoOnContestTask,
                                VideoRepositoryContract   $videoRepository)
    {
        $this->videoRepository  = $videoRepository;
        $this->publishVideoTask = $publishVideoOnContestTask;
    }

    /**
     * Publish video with given uid on daily contest.
     *
     * @param Video  $video
     * @param string $start_at
     */
    public function startContest(Video $video, $start_at)
    {
        $this->publishVideoTask->init($video);

        if ($start_at) {
            $this->publishVideoTask->setDelay($start_at);
        }

        $this->publishVideoTask->run();
    }

    /**
     * Remove given video from daily contest.
     *
     * @param Video $video
     */
    public function stopContest(Video $video)
    {
        $daily = $video->contest_video()->firstOrFail();
        $daily->delete();
    }
}