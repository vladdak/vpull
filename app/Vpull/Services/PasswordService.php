<?php

namespace app\Services;


use App\Http\Requests\Request;

class PasswordService
{
    use RedirectsUsers;
    
    public function sendResetLink(Request $request)
    {
        $request->validate();
    }
}