<?php

namespace App\Vpull\Services;

use App\Contracts\UserRepository;
use App\Jobs\User\UploadProfilePhoto;
use App\Vpull\Resources\ImageResource;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Dingo\Api\Exception\ValidationHttpException;

class UserService
{
    use DispatchesJobs;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * @var ImageResource
     */
    protected $image;


    public function __construct(UserRepository $user, ImageResource $image)
    {
        $this->user  = $user;
        $this->image = $image;
    }

    /**
     * Edit user account details with provided data.
     * 
     * @param Request $request
     * @param integer $id
     */
    public function updateAccountDetails(Request $request, $id)
    {
        $payload = $this->getUpdateAccountCredentials($request, $id);

        $this->validateUpdateAccountRequest($payload, $id);
        
        $this->user->update($id, $payload);
    }

    /**
     * Update user account photo.
     * 
     * @param Request $request
     */
    public function updateAccountPhoto(Request $request)
    {
        $user = $request->user();
        $storage_path = $request->user()->storage();

        $this->image->load($request->file('image'));
        $this->image->setStoragePath($storage_path);

        $request->file('image')->move($this->image->local_path,
                                      $this->image->full_name);

        // TODO: Delete old profile images.
        $this->dispatchNow(new UploadProfilePhoto($this->image, $user));

        $this->user->update($request->user()->id,
            ['photo' => $this->image->filepath('s3')]);
    }
    
    /**
     * Get credentials from request for user model update.
     *
     * @param  Request $request
     * @param  integer $id
     * @return array
     */
    protected function getUpdateAccountCredentials(Request $request, $id)
    {   
        $user = $this->user->find($id);

        $credentials = $request->only('firstname', 'lastname', 'email', 'phone');

        foreach ($credentials as $field => $record)
        {
            if (is_null($record)) {
                $credentials[$field] = $user->$field;
            }
        }

        return $credentials;
    }
    /**
     * Validate user model fields with provided validation rules
     *
     * @param  integer $id
     * @param  array   $payload
     * @throws ValidationHttpException
     */
    protected function validateUpdateAccountRequest($payload, $id)
    {
        $validation = \Validator::make($payload, [
            'firstname' => 'required|alpha|min:3',
            'lastname'  => 'required|alpha|min:3',
            'phone'     => 'required|unique:users,phone,'.$id,
            'email'     => 'required|email|unique:users,email,'.$id,
        ]);

        if ($validation->fails()) {
            throw new ValidationHttpException($validation->errors());
        }
    }
}