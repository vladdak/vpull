<?php

namespace App\Vpull\Services;

use App\User;
use App\Contracts\UserRepository;

use Tymon\JWTAuth\JWTAuth;
use Dingo\Api\Routing\Helpers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthService
{
    use Helpers;

    public function __construct(UserRepository $user, JWTAuth $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }

    /**
     * Attempt login user with given credentials and return token.
     *
     * @param  array  $credentials
     * @return mixed
     */
    public function login($credentials)
    {
        if (!$token = $this->auth->attempt($credentials)) {
            return $this->response->errorUnauthorized('Wrong email or password');
        }

        return $token;
    }

    /**
     * Attempt login user with provided user data and return token.
     *
     * @param  array  $payload
     * @return mixed
     */
    public function socialLogin($payload)
    {
        $user = $this->user->getWith('email', $payload['email']);

        if (!$user->isEmpty())
        {
            $user = $this->user->get($payload);

            if ($user->isEmpty()) {
                throw new UnauthorizedHttpException("Invalid credentials provided!");
            }
        }

        $user  = $this->user->firstOrCreate($payload);
        $token = $this->auth->fromUser($user);

        return $token;
    }

    /**
     * Login user with given User instance
     *
     * @param  User   $user
     * @return string $token
     */
    public function instanceLogin(User $user)
    {
        return $this->auth->fromUser($user);
    }
    
    /**
     * Logout user and invalidate provided token.
     */
    public function logout()
    {
        return $this->auth->invalidate($this->auth->getToken());
    }

    /**
     * Refresh current token and increase it lifetime.
     *
     * @param  string  $token
     * @return string
     */
    public function refreshToken($token)
    {
        try {
            $refreshed = $this->auth->refresh($token);
        }

        catch (JWTException $e) {
            return $this->response->errorInternal('Cannot refresh Token');
        }

        return $refreshed;
    }
}