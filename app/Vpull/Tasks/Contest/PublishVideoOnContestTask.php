<?php

namespace App\Vpull\Tasks\Contest;

use DateTime;
use Carbon\Carbon;

use App\Models\Video;
use App\Contracts\TaskInterface;
use App\Jobs\Video\PublishVideoOnContest;
use App\Vpull\Traits\Eloquent\Datetime as DatetimeTrait;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class PublishVideoOnContestTask implements TaskInterface
{
    use DispatchesJobs, DatetimeTrait;

    /**
     * @var Video
     */
    protected $video;

    /**
     * @var string|Carbon
     */
    protected $start_at;
    
    /**
     * Initialize task with necessary parameter.
     *
     * @param Video $video
     */
    public function init(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute task.
     */
    public function run()
    {
        $this->startContest($this->video, $this->start_at);
    }

    /**
     * Set delay with which video should be published on contest.
     * @param string $start_time
     */
    public function setDelay(string $start_time)
    {
        $this->start_at = $this->toCarbon($start_time);
    }

    /**
     * Publish video on daily contest.
     * 
     * @param  Video    $video
     * @param  DateTime $start_at
     *
     * @throws AccessDeniedHttpException
     * @throws ConflictHttpException
     */
    private function startContest(Video $video, $start_at)
    {
        $user  = $video->user;
        $delay = $this->getDelay($start_at);
            
        if ($user->wonThreeTimes()) {
            throw new AccessDeniedHttpException("You are unable to publish video on the contest until tomorrow.");
        }

        if ($video->onContest()) {
            throw new ConflictHttpException("Video is already on contest");
        }

        $job = (new PublishVideoOnContest($video));
        $job->delay($delay);

        $this->dispatch($job);
    }

    /**
     * Validate the publish time delay.
     *
     * @param  string|Carbon    $time
     * @return DateTime
     * @throws UnprocessableEntityHttpException
     */
    private function getDelay($time): DateTime
    {
        // If date is empty, then we should put video on contest immediately.
        // 5 minutes is an optimal time until video will be uploaded.
        if (empty($time)) $time = Carbon::now()->addMinutes(5);

        // If publish time is less than current time – throw an error.
        if ($time < Carbon::now()) {
            throw new UnprocessableEntityHttpException("Invalid date provided.");
        }

        return new DateTime($time->toDateTimeString());
    }
}