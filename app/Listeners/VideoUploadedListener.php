<?php

namespace App\Listeners;

use App\Events\VideoUploadedEvent;
use App\Contracts\VideoRepositoryContract;

class VideoUploadedListener
{
    /**
     * @var VideoRepositoryContract
     */
    protected $repository;

    /**
     * Create the event listener.
     *
     * @param VideoRepositoryContract $repository
     */
    public function __construct(VideoRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Handle the event.
     *
     * @param  VideoUploadedEvent  $event
     * @return void
     */
    public function handle(VideoUploadedEvent $event)
    {
        // Delete local video file.
        \File::deleteDirectory($event->video->local_path);
        
        // Set Video file as processed.
        $this->repository->setProcessed($event->video->uid);
    }
}
