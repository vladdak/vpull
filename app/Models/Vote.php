<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contest_video_id', 'user_id', 'vote'
    ];

    public static $vote_weights = [
        '1'  => 'up',
        '0'  => 'down'
    ];

    public static function boot()
    {
        parent::boot();

        // Update video rating on vote creation
        static::created(function($vote)
        {
            $vote->contest_video->calculateRating();
        });

        // Update video rating on vote deleting
        static::deleted(function($vote)
        {
            $vote->contest_video->calculateRating();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function contest_video()
    {
        return $this->belongsTo(ContestVideo::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Model action and state methods
    |--------------------------------------------------------------------------
    | There are determined method described model actions or
    | behavior that are not presented in Eloquent Model.
    |
    */
}
