<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['author', 'report_type_id', 'video_id'];

    /*
     |--------------------------------------------------------------------------
     | Model relations section
     |--------------------------------------------------------------------------
     |
     */

    public function report_type()
    {
        return $this->belongsTo(ReportType::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'author');
    }
}