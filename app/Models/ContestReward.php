<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestReward extends Model
{
    protected $table = 'contest_reward';
    protected $fillable = ['amount'];
    
    public static function boot()
    {
        static::creating(function($reward)
        {
            // We can store only one record in contest_reward table,
            // so we just overriding any existing records.
            $existing = $reward->first();
            if (!is_null($existing)) $existing->delete();
        });

        parent::boot();
    }
}
