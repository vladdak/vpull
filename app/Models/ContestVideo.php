<?php

namespace App\Models;

use App\User;

use App\Vpull\Traits\StorageTrait;
use App\Vpull\Traits\Eloquent\Datetime;
use App\Vpull\Traits\Eloquent\Orderable;
use App\Vpull\Traits\Eloquent\VideoVotes;
use App\Vpull\Traits\Eloquent\VideoRelations;

use Illuminate\Database\Eloquent\Model;

class ContestVideo extends Model
{
    use VideoVotes, VideoRelations, Orderable, Datetime, StorageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'user_id', 'rating'
    ];

    /**
     * Fields available for ordering.
     *
     * @var array
     */
    protected $sort = [
        'date' => 'created_at', 'rating' => 'rating'
    ];

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function winner()
    {
        return $this->hasMany(Winner::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Override Model Class methods
    |--------------------------------------------------------------------------
    |
    */

    public function getRouteKeyName()
    {
        return 'uid';
    }


    /*
    |--------------------------------------------------------------------------
    | Model scopes
    |--------------------------------------------------------------------------
    | There are described additional model scopes
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Model action methods
    |--------------------------------------------------------------------------
    | There are determined additional model actions
    | that are not presented in Eloquent Model.
    |
    */

    /**
     * Calculate video rating depending on related votes.
     *
     * @return void
     */
    public function calculateRating()
    {
        $rating = $this->upVotes() - $this->downVotes();

        $this->update(['rating' => $rating]);
    }

    /**
     * Vote for the voteable instance.
     *
     * @param  int   $user_id
     * @return Model
     */
    public function voteUp($user_id)
    {
        $this->votes()->create([
            'vote'    => 1,
            'user_id' => $user_id
        ]);
    }

    /**
     * Vote against votable instance.
     *
     * @param  integer  $user_id
     * @return Model
     */
    public function voteDown($user_id)
    {
        $this->votes()->create([
            'vote'    => 0,
            'user_id' => $user_id
        ]);
    }

    /**
     * Remove vote added for the video.
     *
     * @param  $user_id
     * @return void
     */
    public function voteRemove($user_id)
    {
        $vote = $this->votes()->where('user_id', $user_id)->firstOrFail();

        $vote->delete();
    }

    /*
    |--------------------------------------------------------------------------
    | Model behavior
    |--------------------------------------------------------------------------
    | There are determined methods described additional
    | model behaviors that are not presented in Eloquent Model.
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Model protected methods
    |--------------------------------------------------------------------------
    | This methods may affect model data and must be called only inside
    | the current model class.
    |
    */
}