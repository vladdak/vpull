<?php

namespace App\Models;

use App\Vpull\Traits\Eloquent\Datetime;
use App\Vpull\Traits\Eloquent\Orderable;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    use Orderable, Datetime;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'user_id', 'video_id', 'rating', 'up_votes', 'down_votes', 'created_at'
    ];

    /**
     * Fields available for ordering.
     *
     * @var array
     */
    protected $sort = [
        'date' => 'created_at', 'name' => 'title', 'rating' => 'rating'
    ];

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}