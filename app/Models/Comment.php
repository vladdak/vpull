<?php

namespace App\Models;

use App\User;
use App\Vpull\Traits\Eloquent\Datetime;
use App\Vpull\Traits\Eloquent\Orderable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Datetime, Orderable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uid', 'user_id', 'body'];

    /**
     * Fields available for ordering.
     *
     * @var array
     */
    protected $sort = [
        'date' => 'created_at'
    ];
    
    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Override Model Class methods
    |--------------------------------------------------------------------------
    |
    */

    public function getRouteKeyName()
    {
        return 'uid';
    }

    /*
    |--------------------------------------------------------------------------
    | Model action and state methods
    |--------------------------------------------------------------------------
    | There are determined method described model actions or
    | behavior that are not presented in Eloquent Model.
    |
    */

    /**
     * Add comment for the given video.
     * @param array $attributes
     */
    public static function addComment($attributes)
    {
        self::create([
            'uid'      => uniqid(true),
            'body'     => $attributes['comment'],
            'user_id'  => $attributes['user_id'],
            'video_id' => $attributes['video_id'],
        ]);
    }

    /**
     * Get user full name who posted the comment.
     * @return string
     */
    public function commentator()
    {
        return $this->user->firstname . " " . $this->user->lastname;
    }
}
