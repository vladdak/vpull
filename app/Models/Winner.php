<?php

namespace App\Models;

use App\Vpull\Traits\Eloquent\Datetime;
use App\Vpull\Traits\Eloquent\Orderable;
use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    use Orderable, Datetime;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'user_id', 'video_id', 'feedback'
    ];

    /**
     * Fields available for ordering.
     *
     * @var array
     */
    protected $sort = [
        'date' => 'created_at'
    ];

    /*
    |--------------------------------------------------------------------------
    | Override Model Class methods
    |--------------------------------------------------------------------------
    |
    */

    public function getRouteKeyName()
    {
        return 'id';
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
