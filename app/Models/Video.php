<?php

namespace App\Models;

use App\User;
use App\Models\Scopes\ActiveScope;
use App\Vpull\Traits\Eloquent\Video\ReportRelations;
use App\Vpull\Traits\StorageTrait;
use App\Vpull\Traits\Eloquent\Datetime;
use App\Vpull\Traits\Eloquent\Orderable;
use App\Vpull\Traits\Eloquent\VideoVotes;
use App\Vpull\Traits\Eloquent\VideoRelations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Video extends Model
{
    use VideoVotes, VideoRelations, ReportRelations, Orderable, Datetime, StorageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'user_id', 'title', 'description', 'preview', 'path', 'processed'
    ];

    /**
     * Fields available for ordering.
     *
     * @var array
     */
    protected $sort = [
        'date' => 'created_at', 'name' => 'title',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function contest_video()
    {
        return $this->hasOne(ContestVideo::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Override Model Class methods
    |--------------------------------------------------------------------------
    |
    */

    public function getRouteKeyName()
    {
        return 'uid';
    }

    /*
    |--------------------------------------------------------------------------
    | Model scopes
    |--------------------------------------------------------------------------
    | There are described additional model scopes
    |
    */

    /**
     * Show videos without Active Scope applied.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeWithHidden($query) {
        return $query->withoutGlobalScope(ActiveScope::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Model action methods
    |--------------------------------------------------------------------------
    | There are determined additional model actions
    | that are not presented in Eloquent Model.
    |
    */

    /**
     * Add comment for the current video.
     * 
     * @param string $comment
     * @param int    $user_id
     */
    public function addComment($comment, $user_id)
    {
        $this->comments()->create([
            'uid'     => uniqid(true),
            'body'    => $comment,
            'user_id' => $user_id,
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Model behavior
    |--------------------------------------------------------------------------
    | There are determined methods described additional
    | model behaviors that are not presented in Eloquent Model.
    |
    */
    
    /**
     * Get full name of the video owner.
     * @return string
     */
    public function username()
    {
        return $this->user->fullname();
    }

    /**
     * Get preview url for the current video.
     * @return string
     */
    public function preview()
    {
        return $this->diskCloud()->url($this->preview);
    }

    /**
     * Get storage url for the current video.
     * @return string
     */
    public function url()
    {
        return $this->diskCloud()->url($this->path);
    }

    /*
    |--------------------------------------------------------------------------
    | Model protected methods
    |--------------------------------------------------------------------------
    | This methods may affect model data and must be called only inside
    | the current model class.
    |
    */
}