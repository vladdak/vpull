<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type'];

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}