<?php

namespace App\Jobs\User;

use App\User;
use App\Jobs\Job;
use App\Vpull\Traits\StorageTrait;
use App\Vpull\Resources\ImageResource;

use Storage;

use Dingo\Api\Exception\UpdateResourceFailedException;

class UploadProfilePhoto extends Job
{
    use StorageTrait;

    /**
     * @var ImageResource
     */
    public $image;

    /**
     * @var User
     */
    public $user;

    /**
     * UploadProfilePhoto constructor.
     *
     * @param  ImageResource  $image
     * @param  User           $user
     */
    public function __construct(ImageResource $image, User $user)
    {
        $this->image = $image;
        $this->user  = $user;
    }

    /**
     * Execute the job.
     * Job uploads video and preview on cloud storage.
     *
     */
    public function handle()
    {
        $image = $this->image;


        if (
        Storage::disk('s3-video')->put($image->filepath('s3'),
            fopen($image->filepath('local'), 'r+'), 'public'))
        {
            return true;
        }

        else throw new UpdateResourceFailedException("Cannot upload image.");
    }
}