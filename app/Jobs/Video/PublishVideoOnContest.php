<?php

namespace App\Jobs\Video;

use App\Jobs\Job;
use App\Models\Video;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class PublishVideoOnContest extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Video
     */
    protected $video;

    /**
     * PublishVideoOnContest constructor.
     * @param Video $video
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ConflictHttpException
     */
    public function handle()
    {
        $video = $this->video;

        if ($video->onContest()) $this->delete(); // Delete duplicated job.

        else return $video->contest_video()->create
        ([
            'uid'     => $video->uid,
            'user_id' => $video->user_id
        ]);
    }
}