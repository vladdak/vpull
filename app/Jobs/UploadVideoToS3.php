<?php

namespace App\Jobs;

use Event;
use Storage;
use App\User;
use App\Jobs\Job;
use App\Events\VideoUploadedEvent;
use App\Vpull\Traits\StorageTrait;
use App\Vpull\Resources\VideoResource;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadVideoToS3 extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, StorageTrait;

    /**
     * @var VideoResource
     */
    public $video;

    /**
     * @var User
     */
    public $user;

    /**
     * UploadVideoToS3 constructor.
     *
     * @param  VideoResource  $video
     * @param  User           $user
     */
    public function __construct(VideoResource $video, User $user)
    {
        $this->video = $video;
        $this->user  = $user;
    }

    /**
     * Execute the job.
     * Job uploads video and preview on cloud storage.
     *
     * @return void
     */
    public function handle()
    {
        $video = $this->video;


        if (
            Storage::disk('s3-video')->put($video->filepath('s3'),
                                     fopen($video->filepath('local'), 'r+'), 'public') &&
            Storage::disk('s3-video')->put($video->preview->filepath('s3'),
                                     fopen($video->preview->filepath('local'), 'r+'), 'public'))
        {
            Event::fire(new VideoUploadedEvent($video));
        }
    }
}
