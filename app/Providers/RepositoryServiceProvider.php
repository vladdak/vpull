<?php

namespace App\Providers;

use App\Contracts\UserRepository;
use App\Contracts\VideoRepositoryContract;

use App\Vpull\Repositories\UserRepositoryEloquent;
use App\Vpull\Repositories\VideoRepositoryEloquent;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {

        // Bind Eloquent Video Repository to VideoRepository Interface.
        $this->app->bind(
            VideoRepositoryContract::class,
            VideoRepositoryEloquent::class
        );
        
        // Bind Eloquent User Repository to UserRepository Interface.
        $this->app->bind(
            UserRepository::class,
            UserRepositoryEloquent::class
        );
    }
}