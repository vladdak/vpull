<?php

namespace App\Providers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register API Authorization Exception handler with correct status code.
        $this->app->make('api.exception')->register(function (AuthorizationException $e) {
            abort(401, "You are not allowed to perform this action");
        });

        // Register API Model Not Found Exception handler with correct status code.
        $this->app->make('api.exception')->register(function (ModelNotFoundException $e) {
            abort(404, "No query results found");
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
