<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use App\Vpull\Resources\VideoResource;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class VideoUploadedEvent extends Event
{
    use SerializesModels;

    /**
     * @var VideoResource
     */
    public $video;

    /**
     * Create a new event instance.
     *
     * @param VideoResource   $video
     */
    public function __construct(VideoResource $video)
    {
        $this->video = $video;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
