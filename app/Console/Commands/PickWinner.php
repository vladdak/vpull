<?php

namespace App\Console\Commands;

use App\Vpull\Repositories\WinnerRepository;
use Illuminate\Console\Command;

class PickWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winner:pick';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elect to the daily contest winner';

    /**
     * Winner Repository.
     *
     * @var WinnerRepository
     */
    protected $winner;

    /**
     * Create a new command instance.
     *
     * @param  WinnerRepository $repository
     * @return void
     */
    public function __construct(WinnerRepository $repository)
    {
        $this->winner = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->winner->pickWinner();
    }
}
