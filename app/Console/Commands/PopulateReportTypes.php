<?php namespace App\Console\Commands;

use App\Models\ReportType;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class PopulateReportTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate report_types table with available report types.';

    /**
     * @var ReportType
     */
    protected $model;

    protected $types = [
        'Sexual content',
        'Violent or repulsive content',
        'Hateful or abusive content',
        'Harmful or dangerous acts',
        'Child abuse',
        'Infringes my rights'
    ];

    /**
     * Create a new command instance.
     *
     * @param  ReportType $reportType
     */
    public function __construct(ReportType $reportType)
    {
        $this->model = $reportType;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $results = $this->model->all();

        // We are going to remove all records before populating the table.
        $this->truncate($results);

        foreach ($this->types as $type) {
            $this->model->create(['type' => $type]);
        }

    }

    /**
     * Truncate report_types table.
     *
     * @param  Collection $report_types
     * @return bool
     */
    private function truncate(Collection $report_types)
    {
        if (!$report_types->isEmpty())
        {
            foreach ($report_types as $report_type) {
                $report_type->delete();
            }
        }

        return true;
    }
}
