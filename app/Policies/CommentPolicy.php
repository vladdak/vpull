<?php

namespace App\Policies;

use App\User;
use App\Models\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given comment can be updated by the user.
     *
     * @param  User     $user
     * @param  Comment  $comment
     * @return bool
     */
    public function update(User $user, Comment $comment)
    {
        return $user->id  === $comment->user_id;
    }

    /**
     * Determine if the given comment can be deleted by the user.
     *
     * @param User    $user
     * @param Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment)
    {
        return $user->id  === $comment->user_id;
    }
}
