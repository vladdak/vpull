<?php

namespace App\Policies;

use App\User;
use App\Models\Winner;

use Illuminate\Auth\Access\HandlesAuthorization;

class WinnerPolicy
{
    use HandlesAuthorization;

    /**
     * Check if user is the real contest winner.
     *
     * @param  User   $user
     * @param  Winner $winner
     * @return bool
     */
    public function postFeedback(User $user, Winner $winner)
    {
        return $user->id  === $winner->user_id;
    }
}