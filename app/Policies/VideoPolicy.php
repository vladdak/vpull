<?php

namespace App\Policies;

use App\User;
use App\Models\Video;

use Illuminate\Auth\Access\HandlesAuthorization;

class VideoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given video can be updated by the user.
     *
     * @param  User   $user
     * @param  Video  $video
     * @return bool
     */
    public function update(User $user, Video $video)
    {
        return $user->id  === $video->user_id;
    }

    /**
     * Determine if the given video can be deleted by the user.
     *
     * @param  User   $user
     * @param  Video  $video
     * @return bool
     */
    public function delete(User $user, Video $video)
    {
        return $user->id  === $video->user_id;
    }
}
