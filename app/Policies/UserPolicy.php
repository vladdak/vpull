<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given User and authorized User
     * is the same person.
     *
     * @param  User  $user
     * @param  User  $user
     * @return bool
     */
    public function seeStatistics(User $auth, User $user)
    {
        return $auth->id  === $user->id;
    }
}
