<?php

namespace App;

use App\Models\Vote;
use App\Models\Video;
use App\Models\Comment;
use App\Vpull\Traits\Eloquent\User\WinnerRelation;

use App\Vpull\Traits\StorageTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use WinnerRelation, StorageTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'photo', 'email', 'password',  'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | Model relations section
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get all comments to videos user has ever posted.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get videos for the current user.
     */
    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    /**
     * Get votes for the current user.
     */
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    
    /*
    |--------------------------------------------------------------------------
    | Override Model Class methods
    |--------------------------------------------------------------------------
    |
    */

    public function getRouteKeyName()
    {
        return 'id';
    }

    /*
    |--------------------------------------------------------------------------
    | Model action and state methods
    |--------------------------------------------------------------------------
    | There are described model actions or
    | behaviors that are not presented in Eloquent Model.
    |
    */

    /**
     * Get videos that user has liked.
     *
     * @return boolean  $has_voted
     */
    public function getFavouriteVideos()
    {
        //
    }

    /**
     * Get user avatar url.
     * @return string
     */
    public function avatar()
    {
        if (empty($this->photo)) return null;
        return $this->diskCloud()->url($this->photo);
    }

    /**
     * Get users full name.
     * @return string
     */
    public function fullname()
    {
        return $this->firstname ." ". $this->lastname;
    }

    /**
     * Get storage path for user.
     * @return string
     */
    public function storage()
    {
        return $this->firstname . $this->lastname . "_" . $this->id;
    }
}
