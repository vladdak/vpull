<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Initialize Dingo Api router class.
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api'], function($api) {

    /**
     * Auth routes group.
     */
    $api->group(['prefix' => 'auth'], function($api)
    {
        $api->post('login', [
           'as'   => 'auth.login',
           'uses' => 'AuthController@login',
        ]);

        $api->get('logout', [
           'as'   => 'auth.logout',
           'uses' => 'AuthController@logout',
        ]);

        $api->post('facebook', [
            'as'   => 'auth.facebook-login',
            'uses' => 'AuthController@socialLogin',
        ]);

        $api->post('social', [
            'as'   => 'auth.social-login',
            'uses' => 'AuthController@socialLogin',
        ]);

        $api->put('refresh-token', [
            'as'   => 'auth.refresh-token',
            'uses' => 'AuthController@refreshToken',
        ]);

    });

    /**
     * User routes group.
     */
    $api->group(['prefix' => 'user'], function($api)
    {
        $api->get('{user}', [
            'as'   => 'user.show',
            'uses' => 'UserController@show'
        ]);

        $api->get('{user}/videos', [
            'as'   => 'user.videos',
            'uses' => 'UserController@videos'
        ]);

        $api->get('{user}/statistics', [
            'as'   => 'statistics.get',
            'uses' => 'StatisticsController@get'
        ]);

        $api->post('create', [
            'as'   => 'user.store',
            'uses' => 'UserController@store',
            'middleware' => 'sanitizer',
        ]);
    });

    /**
     * Account routes group.
     */
    $api->group(['prefix' => 'account'], function($api)
    {
        $api->get('/', [
            'as'   => 'account.show',
            'uses' => 'AccountController@show'
        ]);

        $api->post('upload/photo', [
            'as'   => 'account.upload-photo',
            'uses' => 'AccountController@uploadPhoto'
        ]);


        $api->put('update', [
            'as'   => 'account.update',
            'uses' => 'AccountController@update',
            'middleware' => 'sanitizer',
        ]);

        $api->delete('delete', [
            'as'   => 'account.destroy',
            'uses' => 'AccountController@destroy'
        ]);
    });


    $api->group(['prefix' => 'video'], function($api)
    {
        $api->get('feed', [
            'as'   => 'video.feed',
            'uses' => 'VideoFeedController@all'
        ]);

        $api->post('upload', [
            'as'   => 'video.upload',
            'uses' => 'VideoUploadController@upload'
        ]);

        $api->put('{video}/store', [
            'as'   => 'video.store',
            'uses' => 'VideoUploadController@store'
        ]);
        
        $api->get('{video}', [
            'as'   => 'video.show',
            'uses' => 'VideoController@show'
        ]);

        $api->put('{video}/update', [
            'as'   => 'video.update',
            'uses' => 'VideoController@update'
        ]);

        $api->delete('{video}/delete', [
            'as'   => 'video.delete',
            'uses' => 'VideoController@delete'
        ]);

        $api->get('{video}/votes', [
            'as'   => 'video.votes',
            'uses' => 'VideoVoteController@all'
        ]);

        $api->post('{video}/votes/up', [
            'as'   => 'video.vote-up',
            'uses' => 'VideoVoteController@voteUp'
        ]);

        $api->post('{video}/votes/down', [
            'as'   => 'video.vote-down',
            'uses' => 'VideoVoteController@voteDown'
        ]);

        $api->delete('{video}/votes/remove', [
            'as'   => 'video.vote-remove',
            'uses' => 'VideoVoteController@unvote'
        ]);

        $api->get('{video}/comments', [
            'as'   => 'video.comments',
            'uses' => 'VideoCommentController@all'
        ]);

        $api->post('{video}/comments', [
            'as'   => 'video.comments',
            'uses' => 'VideoCommentController@post'
        ]);

        $api->delete('{video}/comments/{comment}/delete', [
            'as'   => 'video.comment-delete',
            'uses' => 'VideoCommentController@delete'
        ]);

        $api->post('{video}/contest/start', [
            'as'   => 'video.start-contest',
            'uses' => 'VideoContestController@startContest'
        ]);

        $api->delete('{video}/contest/cancel', [
            'as'   => 'video.cancel-contest',
            'uses' => 'VideoContestController@cancelContest'
        ]);

        $api->post('{video}/report', [
            'as'   => 'video.report',
            'uses' => 'ReportController@reportVideo'
        ]);
    });

    $api->group(['prefix' => 'report'], function ($api)
    {
        $api->get('all', [
            'as'   => 'report.all',
            'uses' => 'ReportController@getReportTypes'
        ]);
    });

    $api->group(['prefix' => 'contest'], function ($api)
    {
        $api->get('feed', [
            'as'   => 'contest.feed',
            'uses' => 'VideoContestController@getContestFeed'
        ]);

        $api->get('reward', [
            'as'   => 'contest.reward',
            'uses' => 'VideoContestController@getContestReward'
        ]);
    });

    $api->group(['prefix' => 'winner'], function ($api)
    {
        $api->get('today', [
            'as'   => 'winner.today',
            'uses' => 'WinnerController@getWinner'
        ]);

        $api->get('latest/{period?}', [
            'as'   => 'winner.latest',
            'uses' => 'WinnerController@getLatestWinners'
        ]);

        $api->get('archive', [
            'as'   => 'winner.archive',
            'uses' => 'WinnerController@getAllWinners'
        ]);

        $api->post('{winner}/feedback', [
            'as'   => 'winner.feedback',
            'uses' => 'WinnerController@postFeedback'
        ]);
    });
    
});

