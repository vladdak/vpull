<?php

namespace App\Http\Controllers\Api;

use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Contracts\VideoRepositoryContract;
use App\Vpull\Transformers\VideoTransformer;

class VideoFeedController extends BaseController
{
    /**
     * @var VideoRepositoryContract
     */
    protected $repository;

    /**
     * @var VideoTransformer
     */
    protected $transformer;


    public function __construct(VideoRepositoryContract $repository, VideoTransformer $transformer)
    {
        $this->repository  = $repository;
        $this->transformer = $transformer;
    }

    /**
     * Return paginated video feed sorted in specified order.
     *
     * @param  OrderableRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function all(OrderableRequest $request)
    {
        $query = new SortProvider($request);
        $feed  = $this->repository->getVideoFeed($query);

        return $this->response()->paginator($feed, $this->transformer);
    }
}