<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Video;
use App\Http\Requests;
use App\Contracts\UserRepository;
use App\Vpull\Services\AuthService;
use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Contracts\VideoRepositoryContract;
use App\Vpull\Transformers\UserTransformer;
use App\Vpull\Transformers\VideoTransformer;

use Illuminate\Http\Request;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/user")
 */
class UserController extends BaseController
{
    /**
     * @var AuthService
     */
    protected $authservice;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserTransformer
     */
    protected $transformer;

    /**
     * @var VideoTransformer
     */
    protected $videoTransformer;

    /**
     * @var VideoTransformer
     */
    protected $videoRepository;
    
    /**
     * UserController constructor.
     *
     * @param UserRepository  $repository
     * @param AuthService     $service
     * @param UserTransformer $transformer
     * 
     * @param VideoTransformer        $videoTransformer
     * @param VideoRepositoryContract $videoRepository
     */
    public function __construct(UserRepository  $repository,   AuthService $service,
                                UserTransformer $transformer,  VideoTransformer $videoTransformer,
                                VideoRepositoryContract $videoRepository)
    {
        $this->authservice = $service;
        $this->repository  = $repository;
        $this->transformer = $transformer;

        $this->videoTransformer = $videoTransformer;
        $this->videoRepository  = $videoRepository;

        $this->middleware('api.auth', ['only' => ['show', 'videos']]);
    }

    /**
     * Display user profile.
     *
     * @param  int  $id
     * @return \Dingo\Api\Http\Response
     */
    public function show(User $user)
    {
        return $this->response()->item($user, $this->transformer)->statusCode(200);
    }

    /**
     * Store a newly created User.
     * 
     * @param  Requests\RegistrationRequest  $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Requests\RegistrationRequest $request)
    {
        $credentials = $request->all();

        $user  = $this->repository->create($credentials);
        $token = $this->authservice->instanceLogin($user);

        return $this->success(['token' => $token], 'User has been created');
    }

    /**
     * Get videos uploaded by given user.
     *
     * @param  intger  $user_id
     * @return \Dingo\Api\Http\Response
     */
    public function videos(OrderableRequest $request, User $user)
    {
        $hidden = False;

        if ($request->user()->id === $user->id) {
            $hidden = True;
        }

        $query  = new SortProvider($request);
        $videos = $this->videoRepository->getUserVideos($user, $query, $hidden);

        return $this->response()->paginator($videos, $this->videoTransformer)->statusCode(200);
    }
}
