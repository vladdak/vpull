<?php

namespace App\Http\Controllers\Api;

use App\Contracts\VideoRepositoryContract;
use App\Vpull\Actions\ContestActions;
use App\Vpull\Traits\Eloquent\Datetime;
use App\Http\Requests\VideoUpdateRequest;
use App\Http\Requests\VideoUploadRequest;
use App\Vpull\Repositories\ContestRepository;
use App\Vpull\Transformers\VideoTransformer;

class VideoUploadController extends BaseController
{
    use Datetime;

    /**
     * Repository representation for the Video model.
     * @var VideoRepositoryContract
     */
    public $videoRepository;

    /**
     * @var ContestActions
     */
    public $contestActions;

    /**
     * @var VideoTransformer
     */
    public $videoTransformer;

    /**
     * VideoUploadController constructor.
     *
     * @param VideoRepositoryContract  $video
     * @param VideoTransformer         $transformer
     * @param ContestActions           $contestActions
     */
    public function __construct(VideoRepositoryContract $video, VideoTransformer $transformer,
                                ContestActions $contestActions)
    {
        $this->videoRepository   = $video;
        $this->videoTransformer  = $transformer;
        $this->contestActions    = $contestActions;

        $this->middleware('api.auth', ['only' => ['upload', 'store']]);
    }

    /**
     * Upload video on S3 storage.
     *
     * @param  VideoUploadRequest $request
     * @return mixed
     */
    public function upload(VideoUploadRequest $request)
    {
        $uid = $this->videoRepository->upload($request);

        return $this->success(['id' => $uid], 'Video is in uploading process.');
    }

    /**
     * Store video details.
     *
     * @param  VideoUpdateRequest  $request
     * @param  string              $video_uid
     * @return VideoTransformer
     */
    public function store(VideoUpdateRequest $request, $video_uid)
    {
        $video = $this->videoRepository->find($video_uid, $withHidden = True);

        $this->authorize('update', $video);

        $start_at = $request->get('start_at', "");
        $this->contestActions->startContest($video, $start_at);

        $video->update([
            'title'       => $request->title,
            'description' => $request->description,
        ]);

        return $this->response()->item($video, $this->videoTransformer)->statusCode(201);
    }
}
