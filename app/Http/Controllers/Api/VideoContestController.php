<?php

namespace App\Http\Controllers\Api;

use App\Models\ContestReward;
use App\Models\Video;
use App\Http\Requests\Request;
use App\Vpull\Actions\ContestActions;
use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Http\Requests\StartContestRequest;
use App\Vpull\Repositories\ContestRepository;
use App\Vpull\Transformers\ContestVideoTransformer;

class VideoContestController extends BaseController
{
    /**
     * @var ContestActions
     */
    protected $contestActions;

    /**
     * @var ContestRepository
     */
    protected $contestRepository;

    /**
     * @var ContestVideoTransformer
     */
    protected $transformer;

    /**
     * VideoContestController constructor.
     *
     * @param ContestRepository $contestRepository
     * @param ContestVideoTransformer $transformer
     */
    public function __construct(ContestActions $actions, ContestRepository $contestRepository,
                                ContestVideoTransformer $transformer)
    {
        $this->transformer       = $transformer;
        $this->contestActions    = $actions;
        $this->contestRepository = $contestRepository;

        $this->middleware('api.auth', ['except' => ['getContestFeed']]);
    }

    /**
     * Return Feed with current contest members.
     *
     * @param OrderableRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function getContestFeed(OrderableRequest $request)
    {
        $order = new SortProvider($request, 'rating');
        $feed  = $this->contestRepository->feed($order);
        
        return $this->response->paginator($feed, $this->transformer);
    }

    /**
     * Return current contest reward.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getContestReward()
    {
        $reward = ContestReward::first();

        if (is_null($reward)) $reward = ["reward" => 0];
        else $reward = ["reward" => $reward->amount];

        return $this->success($reward);
    }

    /**
     * Publish video on the daily contest.
     *
     * @param  StartContestRequest  $request
     * @param  Video                $video
     * @return \Dingo\Api\Http\Response
     */
    public function startContest(StartContestRequest $request, Video $video)
    {
        $this->authorize('update', $video);

        $start_at = $request->get('start_at', "");
        $this->contestActions->startContest($video, $start_at);

        return $this->success([], 'Your video has been published on the daily contest.');
    }

    /**
     * Take out video from the dailty contest.
     *
     * @param  Video $video
     * @return \Dingo\Api\Http\Response
     */
    public function cancelContest(Video $video)
    {
        $this->authorize('update', $video);
        $this->contestActions->stopContest($video);

        return $this->success([], 'Your video has been taken on out from the daily contest.', 201);
    }
}