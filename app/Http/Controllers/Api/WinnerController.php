<?php

namespace App\Http\Controllers\Api;


use App\Models\Winner;
use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Vpull\Repositories\WinnerRepository;
use App\Vpull\Repositories\ContestRepository;
use App\Vpull\Transformers\WinnerTransformer;
use App\Http\Requests\WinnerFeedbackRequest;

use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WinnerController extends BaseController
{
    /**
     * @var WinnerRepository
     */
    protected $repository;

    /**
     * @var ContestRepository
     */
    protected $contestRepository;

    /**
     * @var WinnerTransformer
     */
    protected $transformer;
    
    /**
     * WinnerController constructor.
     *
     * @param ContestRepository $contestRepository
     * @param WinnerTransformer $transformer
     * @param WinnerRepository  $repository
     */
    public function __construct(WinnerRepository  $repository, ContestRepository $contestRepository,
                                WinnerTransformer $transformer)
    {
        $this->contest     = $contestRepository;
        $this->repository  = $repository;
        $this->transformer = $transformer;
        
        $this->middleware('api.auth', ['only' => 'postFeedback']);
    }

    /**
     * Get today contest winner.
     * @return \Dingo\Api\Http\Response
     */
    public function getWinner()
    {
        $winner = $this->repository->getTodayWinner();

        if (is_null($winner)) {
            throw new NotFoundHttpException("There is no winner for today.");
        }
        
        return $this->response->item($winner, $this->transformer);
    }

    /**
     * Get latest contest winners.
     *
     * @param  OrderableRequest $request
     * @param  string           $period
     * @return \Dingo\Api\Http\Response
     */
    public function getLatestWinners(OrderableRequest $request, $period="week")
    {
        $query_provider = new SortProvider($request, 'date');

        switch ($period)
        {
            case "month": $period = Carbon::now()->subMonth()->toDateString(); break;
            case "week":
            default:      $period = Carbon::now()->subWeek()->toDateString();
        }

        $winners = $this->repository->getLatestWinners($query_provider, $period);
        
        if ($winners->isEmpty()) {
            throw new NotFoundHttpException("No winners found.");
        }
        
        return $this->response->paginator($winners, $this->transformer);
    }

    /**
     * Get all contest winners.
     * 
     * @param  OrderableRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function getAllWinners(OrderableRequest $request)
    {
        $query_provider = new SortProvider($request, 'date');

        $winners = $this->repository->getAllWinners($query_provider);

        if ($winners->isEmpty()) {
            throw new NotFoundHttpException("No winners found.");
        }

        return $this->response->paginator($winners, $this->transformer);
    }

    /**
     * Post feedback for video that has won.
     *
     * @param  WinnerFeedbackRequest $request
     * @param  Winner                $winner
     * @return \Dingo\Api\Http\Response
     */
    public function postFeedback(WinnerFeedbackRequest $request, Winner $winner)
    {
        $this->authorize('postFeedback', $winner);

        $winner->update([
            'feedback' => $request->feedback
        ]);

        return $this->success([],"Your feedback has been saved", 201);
    }
}