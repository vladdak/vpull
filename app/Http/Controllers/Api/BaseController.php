<?php

namespace App\Http\Controllers\Api;

use Gate;
use App\Http\Requests;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    use Helpers;

    /**
     * Return error format response.
     *
     * @param string  $message
     * @param integer $status_code
     * @param \Dingo\Api\Http\Response
     */
    protected function error($message, $status_code)
    {
        return $this->response->error($message, $status_code);
    }

    /**
     * Check if user allowed to perform action on given model.
     *
     * @param  string    $action
     * @param  \Eloquent $model
     * @throws \HttpException
     */
    protected function isAllowed($action, $model)
    {
        if (Gate::denies($action, $model))
        {
            $this->response->errorUnauthorized("Authorization error. You cannot perform this action.");
        }
    }

    /**
     * Return success format response.
     *
     * @param  string  $message
     * @param  array   $data
     * @return \Dingo\Api\Http\Response
     */
    protected function success($data, $message="", $code=200)
    {
        $response = [
            'message'     => $message,
            'data'        => $data,
            'status_code' => $code
        ];

        return $this->response->array($response)->statusCode($code);
    }

    
}
