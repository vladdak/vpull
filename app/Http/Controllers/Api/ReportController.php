<?php
/**
 * Created by PhpStorm.
 * User: vladdakhno
 * Date: 2/8/17
 * Time: 1:13 AM
 */

namespace App\Http\Controllers\Api;

use App\Models\ReportType;
use App\Models\Video;
use App\Models\Report;
use App\Vpull\Actions\ReportActions;

use Illuminate\Http\Request;

class ReportController extends BaseController
{
    public function __construct(Report $report, ReportType $reportType)
    {
        $this->report = $report;
        $this->reportType = $reportType;
        
        $this->middleware('api.auth');
    }

    /**
     * Get available report types and their ids.
     * @return \Dingo\Api\Http\Response
     */
    public function getReportTypes()
    {
        $report_types = $this->reportType->all();
        
        return $this->success($report_types);
    }

    /**
     * Send report for a video with provided uid.
     * 
     * @param  Request $request
     * @param  Video   $video
     * @return \Dingo\Api\Http\Response
     */
    public function reportVideo(Request $request, Video $video)
    {
        $this->validate($request, [
           'report_type' => 'required|numeric' 
        ]);

        $video->reports()->create([
            'report_type_id' => $request->get('report_type'),
            'author' => $this->user()->id,
        ]);

        return $this->success([], "Your report has been added.");
    }
}