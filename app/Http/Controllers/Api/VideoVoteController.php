<?php

namespace App\Http\Controllers\Api;

use App\Models\ContestVideo;
use App\Vpull\Repositories\VoteRepository;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class VideoVoteController extends BaseController
{
    /**
     * @var VoteRepository
     */
    protected $repository;

    /**
     * VideoVoteController constructor.
     *
     * @param VoteRepository  $repository
     */
    public function __construct(VoteRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware('api.auth', ['except' => ['all']]);
    }
    
    /**
     * Get votes for the given video.
     *
     * @param  Request      $request
     * @param  ContestVideo $video
     * @return \Dingo\Api\Http\Response
     */
    public function all(Request $request, ContestVideo $video)
    {
        $votes = $this->repository->getVotes($video);

        return $this->success($votes)->statusCode(200);
    }

    /**
     * Vote for the given video.
     *
     * @param  Request      $request
     * @param  ContestVideo $video
     * @return \Dingo\Api\Http\Response
     */
    public function voteUp(Request $request, ContestVideo $video)
    {
        $this->repository->voteUp($video, $request->user()->id);
        $votes = $this->repository->getVotes($video);

        return $this->success($votes, 'Your vote has been saved.');
    }

    /**
     * Vote against the given video.
     *
     * @param  Request      $request
     * @param  ContestVideo $video
     * @return \Dingo\Api\Http\Response
     */
    public function voteDown(Request $request, ContestVideo $video)
    {
        $this->repository->voteDown($video, $request->user()->id);
        $votes = $this->repository->getVotes($video);

        return $this->success($votes, 'Your vote has been saved')->statusCode(200);
    }

    /**
     * Remove vote from given video.
     *
     * @param  Request      $request
     * @param  ContestVideo $video
     * @return \Dingo\Api\Http\Response
     */
    public function unvote(Request $request, ContestVideo $video)
    {
        $unvoted = (bool) $this->repository->unvote($video, $request->user()->id);

        if (!$unvoted) {
            throw new AccessDeniedHttpException("You were not voting for the current video!");
        }

        return $this->success([], 'Your vote has been taken out.');
    }
}