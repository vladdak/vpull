<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Contracts\UserRepository;
use App\Vpull\Services\UserService;
use App\Vpull\Transformers\UserTransformer;
use App\Http\Requests\UserPhotoUploadRequest;

use Illuminate\Http\Request;


/**
 * Account resource representation.
 *
 * @Resource("Account", uri="/account")
 */
class AccountController extends BaseController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * AccountController constructor.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service, UserRepository $repository, UserTransformer $transformer)
    {
        $this->transformer = $transformer;
        $this->userService = $service;
        $this->userRepository = $repository;

        $this->middleware('api.auth');
    }

    /**
     * Display current user details.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show()
    {
        return $this->response->item($this->user, $this->transformer)->statusCode(200);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request)
    {
        $this->userService->updateAccountDetails($request, $request->user()->id);

        return $this->success([], "Account details have been updated");
    }

    /**
     * Upload account profile photo.
     *
     * @param  UserPhotoUploadRequest   $request
     * @return \Dingo\Api\Http\Response
     */
    public function uploadPhoto(UserPhotoUploadRequest $request)
    {
        $this->userService->updateAccountPhoto($request);

        return $this->success([], "Account profile image has been updated.");
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function destroy()
    {
        $this->userRepository->delete($this->user()->id);

        return $this->response()->noContent();
    }
}
