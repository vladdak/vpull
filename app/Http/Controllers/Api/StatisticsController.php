<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Statistics;
use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Vpull\Transformers\StatisticsTransformer;

use Dingo\Api\Http\Response;

class StatisticsController extends BaseController
{
    /**
     * @var Statistics
     */
    protected $statistics;

    /**
     * @var StatisticsTransformer
     */
    protected $transformer;

    /**
     * StatisticsController constructor.
     *
     * @param Statistics             $statistics
     * @param StatisticsTransformer  $transformer
     */
    public function __construct(Statistics $statistics, StatisticsTransformer $transformer)
    {
        $this->statistics  = $statistics;
        $this->transformer = $transformer;

        $this->middleware('api.auth');
    }

    /**
     * Get video contest statistics for authorized user.
     *
     * @param  OrderableRequest $request
     * @param  User             $user
     * @return Response
     */
    public function get(OrderableRequest $request, User $user)
    {
        $this->authorize('seeStatistics', $user);

        $order = new SortProvider($request, 'rating');
        $order = $order->provide();
        
        $stats = $this->statistics
                      ->where('user_id', $user->id)
                      ->ordered($order['sort'], $order['order'])
                      ->paginate($order['per_page'])
                      ->appends($order);

        return $this->response->paginator($stats, $this->transformer);
    }
}