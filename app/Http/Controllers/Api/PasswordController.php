<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\PasswordResetRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct(PasswordService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    /**
     * Make password reset request.
     * @Get("/remind")
     * @Request({"email": "email"})
     *
     * @param  PasswordResetRequest $request
     * @return void
     */
    public function remind(Request $request)
    {
        $credentials = $request->only(['email']);
        $this->service->sendResetLink($credentials);
    }

    /**
     * Reset user password by provided token
     * @Post("/reset")
     * @Request({"token": "token"})
     *
     * @param  ResetsPasswords $request
     * @return void
     */
    public function reset(PasswordResetRequest $request)
    {

    }
    
}