<?php

namespace App\Http\Controllers\Api;

use App\Models\Video;
use App\Http\Requests;
use App\Models\Comment;
use App\Vpull\Providers\SortProvider;
use App\Http\Requests\OrderableRequest;
use App\Http\Requests\PostCommentRequest;
use App\Contracts\VideoRepositoryContract;
use App\Vpull\Transformers\CommentsTransformer;

use Illuminate\Http\Request;

class VideoCommentController extends BaseController
{
    /**
     * @var Comment
     */
    protected $model;

    /**
     * @var VideoRepositoryContract
     */
    protected $videoRepository;

    /**
     * Transformer presenter for the Comment model.
     * @var CommentsTransformer
     */
    protected $commentTransformer;

    public function __construct(Comment $comment, VideoRepositoryContract $videoRepository, CommentsTransformer $transformer)
    {
        $this->model = $comment;
        $this->videoRepository = $videoRepository;
        $this->commentTransformer = $transformer;

        $this->middleware('api.auth', ['only' => ['post', 'update', 'delete']]);
    }

    /**
     * Get paginated comments for the given video.
     *
     * @param  OrderableRequest  $request
     * @param  Video             $video
     * @return \Dingo\Api\Http\Response
     */
    public function all(OrderableRequest $request, Video $video)
    {
        $query = new SortProvider($request);

        $comments = $this->videoRepository->getComments($video, $query);

        return $this->response->paginator($comments, $this->commentTransformer);
    }

    /**
     * Post new comment to given video.
     *
     * @param  PostCommentRequest $request
     * @param  Video              $video
     * @return \Dingo\Api\Http\Response
     */
    public function post(PostCommentRequest $request, Video $video)
    {
        $video->addComment($request->comment, $request->user()->id);

        return $this->success([], "Comment for video '{$video->title}' has been added");
    }
    
    /**
     * Delete commentary.
     *
     * @param  Request    $request
     * @param  Video      $video
     * @param  Comment    $comment
     * @return \Dingo\Api\Http\Response
     * @throws \Exception
     */
    public function delete(Request $request, Video $video, Comment $comment)
    {
        $this->authorize('delete', $comment);

        if ($comment->delete())
        {
            return $this->success([], 'Comment has been deleted');
        }
    }
}
