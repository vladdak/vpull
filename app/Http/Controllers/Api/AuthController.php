<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Requests;
use App\Contracts\UserRepository;
use App\Vpull\Services\AuthService;

use Illuminate\Http\Request;

/**
 * Authentication resource representation.
 *
 * @Resource("Auth", uri="/auth")
 */
class AuthController extends BaseController
{

    /**
     * Current request class instance.
     * @var Request
     */
    protected $request;

    /**
     * User model instance.
     * @var mixed
     */
    protected $user;

    public function __construct(Request $request, UserRepository $repository, AuthService $service)
    {
        $this->middleware('api.auth', ['only' => ['logout']]);

        $this->request     = $request;
        $this->repository  = $repository;
        $this->authservice = $service;
    }

    /**
     * Authorize user with given credentials and return token.
     *
     * @param  Requests\AuthRequest $request
     * @return mixed
     */
    public function login(Requests\AuthRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $token = $this->authservice->login($credentials);
        
        return $this->success(['token' => $token], 'User has logged in');
    }

    /**
     * Authorize user with provided user data and return token.
     *
     * @param  Requests\SocialAuthRequest $request
     * @return mixed
     */
    public function socialLogin(Requests\SocialAuthRequest $request)
    {
        $payload = $request->all();
        $token = $this->authservice->socialLogin($payload);

        return $this->success(['token' => $token], 'User has logged in');
    }

    /**
     * Logout user and invalidate his token.
     * @return mixed
     */
    public function logout()
    {
        $this->authservice->logout();
        
        return $this->success([], 'User has been logged out');
    }

    /**
     * Refresh current token and increase it lifetime.
     *
     * @param  Requests\RefreshTokenRequest $request
     * @return mixed
     */
    public function refreshToken(Requests\RefreshTokenRequest $request)
    {
        $token = $request->token;
        $refreshed = $this->authservice->refreshToken($token);
        
        return $this->success(['token'=> $refreshed], 'Token has been updated');
    }
}
