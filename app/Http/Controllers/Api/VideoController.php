<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Video;
use App\Http\Requests\VideoUpdateRequest;
use App\Contracts\VideoRepositoryContract;
use App\Vpull\Transformers\VideoTransformer;
use App\Vpull\Repositories\ContestRepository;

use Illuminate\Http\Request;

class VideoController extends BaseController
{
    /**
     * Repository representation for the Video model.
     * @var VideoRepositoryContract
     */
    public $videoRepository;

    /**
     * Fractal transformer for the Video model
     * @var VideoTransformer
     */
    public $videoTransformer;

    /**
     * Repository representation for the Contest model.
     * @var ContestRepository
     */
    public $contestRepository;

    /**
     * VideoController constructor.
     *
     * @param VideoRepositoryContract $videoRepository
     * @param VideoTransformer   $transformer
     * @param ContestRepository  $contestRepository
     */
    public function __construct(VideoRepositoryContract $videoRepository, VideoTransformer $transformer,
                                ContestRepository $contestRepository)
    {
        $this->videoTransformer  = $transformer;
        $this->videoRepository   = $videoRepository;
        $this->contestRepository = $contestRepository;

        $this->middleware('api.auth', ['only' => ['delete', 'update']]);
    }

    /**
     * Delete video.
     *
     * @param  Request  $request
     * @param  Video    $video
     * @return \Dingo\Api\Http\Response|void
     */
    public function delete(Request $request, Video $video)
    {
        $this->authorize('delete', $video);

        if ($this->videoRepository->delete($video)) {
            return $this->success([], "Video {$video->title} has been deleted.");
        }

        else {
            return $this->error("Cannot delete video", 400);
        }
    }
    
    /**
     * Get video url by provided uid.
     *
     * @param  Video  $video
     * @return mixed
     */
    public function show(Video $video)
    {
        return $this->response->item($video, $this->videoTransformer)->statusCode(200);
    }

    /**
     * Update video details.
     *
     * @param  VideoUpdateRequest  $request
     * @param  string              $video_uid
     * @return VideoTransformer
     */
    public function update(VideoUpdateRequest $request, $video_uid)
    {
        $video = $this->videoRepository->find($video_uid, $withHidden = True);

        $this->authorize('update', $video);

        $video->update([
            'title'       => $request->title,
            'description' => $request->description,
        ]);

        return $this->response()->item($video, $this->videoTransformer)->statusCode(201);
    }
}
