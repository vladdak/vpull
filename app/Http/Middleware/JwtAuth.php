<?php namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;
use Exception;

class JwtAuthMiddleware {

    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
        } 
        
        catch (Exception $e) 
        {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
            {
                return response()->json(['error'=>'Invalid token']);
            }

            elseif ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
            {
                return response()->json(['error'=>'Token expired']);
            }

            else
            {
                return response()->json(['error'=>'Something went wrong']);
            }

        }

        return $next($request);
    }

}
