<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Dingo\Api\Http\FormRequest;

class VideoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'min:3|max:255',
            'description' => 'max:2000',
            'start_at'    => 'date_format: Y-m-d H:i:s'
        ];
    }
}
