<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class OrderableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order'    => 'in:asc,desc',
            'sort'     => 'in:date,name,rating',
            'per_page' => 'numeric|max:50'
        ];
    }
}
