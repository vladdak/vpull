<?php

namespace App\Contracts;


interface TaskInterface
{
    public function run();
}